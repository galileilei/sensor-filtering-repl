from rest_framework import serializers
from .models import Todo


class TodoSerializer(serializers.ModelSerializer):
    """
    convert model instance to JSON
    """

    class Meta:
        model = Todo
        fields = ("id", "title", "description", "completed")
