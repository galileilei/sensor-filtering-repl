from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from .serializer import TodoSerializer
from .models import Todo


class TodoView(viewsets.ModelViewSet):
    """
    viewsets provides the implementation for CRUD operations by default.
    we need to specify:
    1. the serializer to convert class instance to JSON, and
    2. the set of query we are allowed.
    """

    serializer_class = TodoSerializer
    queryset = Todo.objects.all()