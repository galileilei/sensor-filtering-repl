# Frontend rendering data using React + Typescript

This is a visualization of filtering algorithm, rendered in a browser. The aim is two-fold:

1. learn React + Typescript for modern UI experience in a browser, and
2. a simple REPL for testing more algorithm if the need ever arises.

## Overview

This project is created with `npx create-react-app frontend --template typescript`, and this is what is created:

```bash
.
├── ./node_modules          # external JS modules
├── ./package.json          # declare this as a JS modules
├── ./package-lock.json     #
├── ./public                # static content of the frontend
├── ./README.md             # this document
├── ./README.md.bak         # default doc from `npx create-react-app frontend --template typescript`
├── ./src                   # dynamic content of the frontend
└── ./tsconfig.json         # various configration option for typescript
```

## Layout of `src`

Two points worth mentioning:

- React recommends PascalCase for files that export React Component, because it uses lowercase for intrinsic element (div, p),
- use lowercase for folders, and camelCase for instance of React Component.

With that being said, the `src` is structured as:

```bash
.
├── ./api                   # to be removed
├── ./components            # where App lives as Layout
├── ./dashboard             # handles all `/dashboard/*` request
├── ./index.tsx             # entry point, corresponds with `../public/index.html`
├── ./logo.svg              # to be removed
├── ./pages                 # single static pages
├── ./react-app-env.d.ts    # to be removed
├── ./services              # commmon services for all other modules
└── ./todo                  # handles all `/todo/*` request
```

##
