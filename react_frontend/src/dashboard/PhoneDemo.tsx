import React from "react";
import { StaticChart } from "./VictoryDemo";
import VectorPlot from "./VectorPlot";
import TimeSeriesPlot from "./TimeSeriesPlot";
import "./style.css";

const PlaceHolder = StaticChart;

const defaultNames: [string, string, string] = ["x", "y", "z"];

function Sensor(prop: Prop) {
  function getSensor(dtype: string = prop.dtype) {
    function getEndpoint(dtype: string) {
      const prefix = "/api/phone/latest"; // does not need localhost:8000 because we proxy the request
      return prefix + "/" + dtype;
    }
    return (
      <VectorPlot
        names={defaultNames}
        title={dtype}
        endpoint={getEndpoint(dtype)}
        refreshInUrl={200}
      />
    );
  }

  return getSensor(prop.dtype);
}

interface Prop {
  dtype: string;
}

function PhoneDemo() {
  return (
    <div className="twocols">
      <div className="leftcol">
        <Sensor dtype="accelerometer" />
        <Sensor dtype="gyroscope" />
        <Sensor dtype="magnetic_field" />
      </div>
      <div className="rightcol">
        <Sensor dtype="orientation" />
        <Sensor dtype="rotation_vector" />
      </div>
    </div>
  );
}

export default PhoneDemo;
