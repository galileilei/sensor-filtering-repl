import Axios from "axios";
import React from "react";
import {
  VictoryChart,
  VictoryLegend,
  VictoryLine,
  VictoryScatter,
  VictoryTheme,
  VictoryAxis,
} from "victory";

interface VectorPlotProp {
  title: string;
  names: [string, string, string];
  endpoint: string;
  refreshInUrl: number;
  size?: { height: number; width: number };
  domainX?: [number, number];
  domainY?: [number, number];
}

type Value = number[];

interface Payload {
  timestamp: number;
  payload: Value;
}

function SingleLine(index: number = 0, color: string, data?: Payload[]) {
  return [
    <VictoryLine
      interpolation="catmullRom"
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      style={{ data: { stroke: color } }}
    />,
    <VictoryScatter
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      size={5}
      style={{ data: { fill: color } }}
    />,
  ];
}

function VectorPlot(props: VectorPlotProp) {
  const colors = ["#BC243C", "#5B5EA6", "#88B04B"];
  const pointType: string[] = Array(3).fill("circle");
  const pointStyle = colors.map(function (e, i) {
    return { fill: e, symbol: pointType[i] };
  });
  const [domainXRange, setDomainXRange] = React.useState<[number, number]>(
    props.domainX ?? [-10, 10]
  );
  const [domainYRange, setDomainYRange] = React.useState<[number, number]>(
    props.domainY ?? [-10, 10]
  );
  const [data, setData] = React.useState<Payload[]>();
  const [refreshInterval, setRefreshInterval] = React.useState(
    props.refreshInUrl
  );
  const [goLive, toggleGoLive] = React.useState<boolean>(false);

  const height = props.size?.height ?? 200;
  const width = props.size?.width ?? 800;

  function fetchMetrics() {
    Axios.get<Payload[]>(props.endpoint)
      .then((res) => setData(res.data as Payload[]))
      .catch((err) => console.log(err));
    updateRange();
  }

  function updateRange() {
    if (typeof data === "undefined") return;
    const xmin = Math.min(...data.map((e) => e.timestamp));
    const xmax = Math.max(...data.map((e) => e.timestamp));
    const ymin = Math.min(
      ...data.map((e) => e.payload[0]),
      ...data.map((e) => e.payload[1]),
      ...data.map((e) => e.payload[2])
    );
    const ymax = Math.max(
      ...data.map((e) => e.payload[0]),
      ...data.map((e) => e.payload[1]),
      ...data.map((e) => e.payload[2])
    );
    const xrange = xmax - xmin;
    const yrange = ymax - ymin;
    setDomainXRange([xmin - xrange * 0.1, xmax + xrange * 0.1]);
    setDomainYRange([ymin - yrange * 0.1, ymax + yrange * 0.1]);
  }

  React.useEffect(() => {
    if (goLive && refreshInterval > 0) {
      const interval = setInterval(fetchMetrics, refreshInterval);
      return () => clearInterval(interval);
    }
  });

  return (
    <div>
      <h1
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {props.title}
      </h1>
      <br />

      <input
        type="checkbox"
        id="golive"
        value={goLive ? 1 : 0}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          const status = goLive;
          toggleGoLive(!status);
        }}
      ></input>
      <label htmlFor="golive"> Go live now!</label>

      <input
        type="range"
        id="refreshrate"
        min={100}
        max={1000}
        value={refreshInterval}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          console.log(`value is ${event.target.value}`);
          setRefreshInterval(+event.target.value);
        }}
      ></input>
      <label htmlFor="refreshrate"> refresh rate = {refreshInterval} ms </label>
      <br />
      <VictoryChart
        polar={false}
        theme={VictoryTheme.material}
        height={height}
        width={width}
      >
        <VictoryAxis
          crossAxis
          width={width}
          height={height}
          domain={domainXRange}
        />
        <VictoryAxis
          dependentAxis
          crossAxis
          width={width}
          height={height}
          domain={domainYRange}
        />
        {[...Array(3).keys()].map((i) => SingleLine(i, colors[i], data)).flat()}
        <VictoryLegend
          x={width * 0.9}
          y={height * 0.1}
          title="Legend"
          centerTitle
          orientation="vertical"
          gutter={20}
          data={[...Array(3).keys()].map((i) => ({
            name: props.names[i],
            symbol: pointStyle[i],
          }))}
        />
      </VictoryChart>
    </div>
  );
}

export default VectorPlot;
