export { default as Dashboard } from "./Dashboard";
export { default as VictoryDemo } from "./VictoryDemo";
export { default as LinePlot } from "./LinePlot";
export { default as VectorPlot } from "./VectorPlot";
