import * as React from "react";
import VictoryDemo from "./VictoryDemo";
import LinePlot from "./LinePlot";
import VectorPlot from "./VectorPlot";
import PhoneDemo from "./PhoneDemo";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import MadgwickDemo from "./MadgwickDemo";

function Dashboard() {
  let { path, url } = useRouteMatch();
  console.log(`path is ${path}, url is ${url}`);

  return (
    <div>
      <h1> url is {url} </h1>
      <h2> path is {path} </h2>
      <Switch>
        <Route exact path={`${url}/vectorplot`}>
          <VectorPlot
            title="testing"
            names={["x", "y", "z"]}
            endpoint="localhost:8000/api/phone/latest/accelerometer"
            refreshInUrl={200}
            domainX={[-10, 10]}
            domainY={[-10, 10]}
          />
        </Route>
        <Route exact path={`${url}/lineplot`}>
          <LinePlot />
        </Route>
        <Route exact path={`${url}/victorydemo`}>
          <VictoryDemo />
        </Route>
        <Route exact path={`${url}/phonedemo`}>
          <PhoneDemo />
        </Route>
        <Route exact path={`${url}/madgwick`}>
          <MadgwickDemo />
        </Route>
      </Switch>
    </div>
  );
}

export default Dashboard;
