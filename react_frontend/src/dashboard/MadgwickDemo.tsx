import React from "react";
import TimeSeriesPlot from "./TimeSeriesPlot";
import { QueryClient, QueryClientProvider } from "react-query";
import "./style.css";

interface SliderProp {
  goLive: boolean;
  toggleGoLive: React.Dispatch<React.SetStateAction<boolean>>;
  refreshInterval: number;
  setRefreshInterval: React.Dispatch<React.SetStateAction<number>>;
}

function Slider(props: SliderProp) {
  return (
    <React.Fragment>
      <input
        type="checkbox"
        id="golive"
        value={props.goLive ? 1 : 0}
        onChange={(event) => {
          const status = props.goLive;
          props.toggleGoLive(!status);
        }}
      />
      <label htmlFor="golive"> Go Live Now! </label>
      <input
        type="range"
        id="refreshrate"
        min={100}
        max={1000}
        value={props.refreshInterval}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          console.log(`value is ${event.target.value}`);
          props.setRefreshInterval(+event.target.value);
        }}
      ></input>
      <label htmlFor="refreshrate">
        {" "}
        refresh rate = {props.refreshInterval} ms{" "}
      </label>
    </React.Fragment>
  );
}

interface LiveSensorProp {
  dtype: string;
  names: string[];
  refreshInUrl: number;
  goLive: boolean;
}

function LiveSensor(props: LiveSensorProp) {
  const { goLive, refreshInUrl, names, dtype } = props;
  const api_port = "/api/madgwick/latest/" + dtype;
  return (
    <div>
      <p>
        Debug: {refreshInUrl} and {goLive ? "live" : "not live"}
      </p>
      <TimeSeriesPlot
        title={dtype}
        names={names}
        endpoint={api_port}
        refreshMs={refreshInUrl}
        goLive={goLive}
      />
    </div>
  );
}

const queryClient = new QueryClient();

function MadgwickDemo() {
  const [goLive, toggleGoLive] = React.useState<boolean>(false);
  const [refreshInterval, setRefreshInterval] = React.useState<number>(200);

  return (
    <div>
      <Slider
        goLive={goLive}
        toggleGoLive={toggleGoLive}
        refreshInterval={refreshInterval}
        setRefreshInterval={setRefreshInterval}
      />
      <QueryClientProvider client={queryClient}>
        <div className="twocols">
          <div className="leftcol">
            <LiveSensor
              dtype="accelerometer"
              names={["x", "y", "z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
            <LiveSensor
              dtype="gyroscope"
              names={["x", "y", "z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
            <LiveSensor
              dtype="magnetic_field"
              names={["x", "y", "z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
          </div>
          <div className="rightcol">
            <LiveSensor
              dtype="rotation_vector"
              names={["about x", "about y", "about z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
            <LiveSensor
              dtype="rotation_post"
              names={["about x", "about y", "about z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
            <LiveSensor
              dtype="quaternion"
              names={["v", "x", "y", "z"]}
              refreshInUrl={refreshInterval}
              goLive={goLive}
            />
          </div>
        </div>
      </QueryClientProvider>
    </div>
  );
}

export default MadgwickDemo;
