import * as React from "react";
import {
  VictoryChart,
  VictoryLine,
  VictoryScatter,
  InterpolationPropType,
  VictoryAxis,
  VictoryTheme,
} from "victory";

const data = [
  { x: 1000000, y: 0 },
  { x: 1000001, y: 2 },
  { x: 1000002, y: 1 },
  { x: 1000003, y: 4 },
  { x: 1000004, y: 3 },
  { x: 1000005, y: 5 },
];

const cartesianInterpolations: Array<InterpolationPropType> = [
  "basis",
  "bundle",
  "cardinal",
  "catmullRom",
  "linear",
  "monotoneX",
  "monotoneY",
  "natural",
  "step",
  "stepAfter",
  "stepBefore",
];

const polarInterpolations: Array<InterpolationPropType> = [
  "basis",
  "cardinal",
  "catmullRom",
  "linear",
];

interface InterpolationSelectProp {
  currentValue: InterpolationPropType;
  values: Array<InterpolationPropType>;
}

function InterpolationSelect(props: InterpolationSelectProp) {
  const [
    interpolation,
    setInterpolation,
  ] = React.useState<InterpolationPropType>(props.currentValue);

  return (
    <select
      onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
        setInterpolation(event.target.value as InterpolationPropType);
      }}
      value={interpolation}
      style={{ width: 75 }}
    >
      {props.values.map((value) => (
        <option value={value} key={value}>
          {value}
        </option>
      ))}
    </select>
  );
}

/* change the following to functional components */

function StaticChart() {
  const [
    interpolation,
    setInterpolation,
  ] = React.useState<InterpolationPropType>("linear");

  const [polar, togglePolar] = React.useState<boolean>(false);

  return (
    <div className="victorychart">
      <h1> Placeholder </h1>
      <InterpolationSelect
        currentValue={interpolation}
        values={polar ? polarInterpolations : cartesianInterpolations}
      />
      <input
        type="checkbox"
        id="polar"
        value={polar ? 1 : 0}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          togglePolar(!polar);
          setInterpolation("linear");
        }}
        style={{ marginLeft: 25, marginRight: 5 }}
      />
      <label htmlFor="polar">polar</label>
      <VictoryChart
        polar={polar}
        height={800}
        width={1200}
        theme={VictoryTheme.material}
      >
        <VictoryLine
          interpolation={interpolation}
          data={data}
          style={{ data: { stroke: "#c43a31" } }}
        />
        <VictoryScatter
          data={data}
          size={5}
          style={{ data: { fill: "#c43a31" } }}
        />
        <VictoryAxis
          crossAxis
          width={800}
          height={1200}
          domain={[-10, 10]}
          offsetY={600}
        />
        <VictoryAxis
          dependentAxis
          crossAxis
          width={800}
          height={1200}
          domain={[-20, 20]}
        />
      </VictoryChart>
    </div>
  );
}

function VictoryDemo() {
  return (
    <div className="twocols">
      <div className="leftcol">
        <StaticChart />
        <StaticChart />
        <StaticChart />
      </div>
      <div className="rightcol">
        <StaticChart />
        <StaticChart />
        <StaticChart />
      </div>
    </div>
  );
}

export { StaticChart };
export default VictoryDemo;
