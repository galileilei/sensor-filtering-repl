import {useRef, useEffect, useState} from "react";
import Axios from "axios";

export function useInterval(callback: Function, delay: number): void {
  const savedCallback = useRef<Function>(() => {});

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    const id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}
  
export function useQuery<T>(initialData: T, endpoint: string, refreshMs: number | "false"): T {
  const [data, setData] = useState<T|undefined>(initialData);
  
      useInterval(() => {
          if (typeof refreshMs !== 'number') return;
          Axios.get(endpoint)
          .then((res) => setData(res.data))
          .catch((err) =>console.log);
      }, refreshMs as number);

      return data as T;
}