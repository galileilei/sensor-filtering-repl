import React from "react";
import Main from "./Main";
import SideBar from "./SideBar";
import { Controller, dataContext } from "./Controller";

export function Layout() {
  return (
    <>
      <SideBar />
      <Main />
    </>
  );
}
