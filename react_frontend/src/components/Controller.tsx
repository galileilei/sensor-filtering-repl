import { Slider, Checkbox } from "@material-ui/core";
import React from "react";
import { useQuery } from "./hooks";

const range = (start: number, end: number, step: number = 1): number[] => {
  var result = [start],
    curr = start;
  while (curr < end) {
    result.push((curr += step));
  }
  return result;
};

function framerateInMs(start: number = 10, end: number = 20, step: number = 1) {
  const marks = range(start, end, step).map((x) => 1000.0 / x);
  const result = marks.map((x) => ({ value: x, label: x.toString() + " FPS" }));
  return result;
}

interface FramerateSliderProp {
  start?: number;
  end?: number;
  step?: number;
  value: number;
  disabled?: boolean;
  setValue: React.Dispatch<React.SetStateAction<number>>;
}
function FramerateSlider({
  start = 10,
  end = 20,
  step = 1,
  disabled = false,
  value = 15,
  setValue,
}: FramerateSliderProp): JSX.Element {
  const marks = framerateInMs(start, end, step);

  return (
    <div>
      <Slider
        defaultValue={value}
        step={null}
        marks={marks}
        disabled={disabled}
        aria-labelledby="discrete-slider-restrict"
        onChange={(e: React.ChangeEvent<{}>, newValue: number | number[]) =>
          setValue((1000.0 / (newValue as number)) as number)
        }
      ></Slider>
    </div>
  );
}

interface LiveButtonProp {
  value: boolean;
  setValue: React.Dispatch<React.SetStateAction<boolean>>;
}

function LiveButton({ value, setValue }: LiveButtonProp) {
  return (
    <Checkbox
      checked={value}
      onChange={(e: React.ChangeEvent) => setValue(!value)}
    ></Checkbox>
  );
}

// TODO: use backend query_since() backend
export const dataContext = React.createContext({});

export function Controller({ children }: React.PropsWithChildren<{}>) {
  const data = useQuery({}, "/api/", "false");
  const [goLive, toggleGoLive] = React.useState<boolean>(false);
  const [refreshInMs, setRefreshInMs] = React.useState<number>(15);
  return (
    <div>
      <FramerateSlider value={refreshInMs} setValue={setRefreshInMs} />
      <LiveButton value={goLive} setValue={toggleGoLive} />
      <dataContext.Provider value={data}>{children}</dataContext.Provider>
    </div>
  );
}
