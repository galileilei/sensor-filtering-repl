import React from "react";
import { dataContext } from "./Controller";
import {
  VictoryChart,
  VictoryLegend,
  VictoryLine,
  VictoryScatter,
  VictoryTheme,
  VictoryAxis,
} from "victory";

export function Plotter() {
  const data = React.useContext(dataContext);

  return <div> data is {data} </div>;
}

interface TimeSeriesPlotProp {
  title: string;
  names: string[];
  size?: { height?: number; width?: number };
}

//TODO
function getData(data: any, dtype: string): any {
  return data[dtype];
}

function defaultPointStyle() {
  const colors = ["#BC243C", "#5B5EA6", "#88B04B"];
  const pointType: string[] = Array(3).fill("circle");
  const pointStyle = colors.map(function (e, i) {
    return { fill: e, symbol: pointType[i] };
  });
  return { colors: colors, pointStyle: pointStyle };
}

function SingleLine(index: number = 0, color: string, data?: Payload[]) {
  return [
    <VictoryLine
      interpolation="catmullRom"
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      style={{ data: { stroke: color } }}
    />,
    <VictoryScatter
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      size={5}
      style={{ data: { fill: color } }}
    />,
  ];
}

type Range = [number, number];
function getRange(data: any[]): { xrange: Range; yrange: Range } {
  const xvals = data.map((e) => e.timestamp);
  const yvals = data.map((e) => e.payload).flat();
  const xmin = Math.min(...xvals);
  const ymin = Math.min(...yvals);
  const xmax = Math.max(...xvals);
  const ymax = Math.max(...yvals);
  const xrange = xmax - xmin;
  const yrange = ymax - ymin;
  const x: Range = [xmin - xrange * 0.1, xmax + xrange * 0.1];
  const y: Range = [ymin - yrange * 0.1, ymax + yrange * 0.1];
  return { xrange: x, yrange: y };
}

export function TimeSeriesPlot({
  title,
  names,
  size = {},
}: TimeSeriesPlotProp) {
  const { height = 200, width = 200 } = size;
  const allData = React.useContext(dataContext);
  const data = getData(allData, title);
  const { xrange, yrange } = getRange(data);
  const { colors, pointStyle } = defaultPointStyle();

  return (
    <div>
      <VictoryChart
        polar={false}
        theme={VictoryTheme.material}
        height={height}
        width={width}
      >
        <VictoryAxis crossAxis width={width} height={height} domain={xrange} />
        <VictoryAxis
          dependentAxis
          crossAxis
          width={width}
          height={height}
          domain={yrange}
        />
        {/* various lines */}
        {[...Array(names).keys()]
          .map((i) => SingleLine(i, colors[i] as string, data))
          .flat()}
        {/* the legend in the top right corner */}
        <VictoryLegend
          x={width * 0.9}
          y={height * 0.1}
          title="Legend"
          centerTitle
          orientation="vertical"
          gutter={20}
          data={[...Array(names).keys()].map((i) => ({
            name: names[i],
            symbol: pointStyle[i],
          }))}
        />
      </VictoryChart>
    </div>
  );
}
