import { Link } from "react-router-dom";
import { Button, Toolbar, AppBar } from "@material-ui/core";

interface muiLinkProp {
  to: string;
  text: string;
}

function MuiLink({ to, text }: muiLinkProp) {
  return (
    <Button component={Link} to={to}>
      {text}
    </Button>
  );
}

interface NavigationProp {
  links: muiLinkProp[];
  prefix?: string;
}

const mainNav: NavigationProp = {
  prefix: "",
  links: [
    { to: "/", text: "Home" },
    { to: "/todo", text: "Todo App" },
    { to: "/about", text: "About" },
    { to: "/dashboard", text: "Dashboard" },
  ],
};

const dashboardNav: NavigationProp = {
  prefix: "/dashboard",
  links: [
    { to: "/victory", text: "Victory Static Demo" },
    { to: "/madgwick", text: "Madgwick Filter" },
    { to: "/phone", text: "Phone Demo" },
  ],
};

function NavBar({ prefix, links }: NavigationProp): JSX.Element {
  return (
    <AppBar>
      <Toolbar>
        {links.map((l) => (
          <MuiLink to={prefix + l.to} text={l.text} />
        ))}
      </Toolbar>
    </AppBar>
  );
}

export const MainNavigation = () => NavBar(mainNav);
export const DashboardNavigation = () => NavBar(dashboardNav);
