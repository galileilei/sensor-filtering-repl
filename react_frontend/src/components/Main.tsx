import React from "react";
import { Switch, Route } from "react-router-dom";
import { Todo } from "../todo";
import * as Pages from "../pages";
import { Dashboard } from "../dashboard";
import "./Main.css";

function Main() {
  return (
    <div className="main">
      {/* A <switch> looks through its childern <Route>s and render the first match */}
      <Switch>
        <Route exact path="/" component={Pages.Home} />
        <Route exact path="/todo" component={Todo} />
        <Route exact path="/about" component={Pages.About} />
        <Route exact path="/threedemo" component={Pages.ThreeDemo} />
        <Route path="/dashboard" component={Dashboard} />
      </Switch>
    </div>
  );
}

export default Main;
