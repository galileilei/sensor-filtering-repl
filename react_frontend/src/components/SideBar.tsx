import React from "react";
import { Link } from "react-router-dom";
import "./SideBar.css";

const SideBar: React.FC<{}> = () => {
  return (
    <nav>
      <li>
        <Link to="/"> Home </Link>
      </li>
      <li>
        <Link to="/todo"> Todo App </Link>
      </li>
      <li>
        <Link to="/about"> About </Link>
      </li>
      <li>
        <Link to="/threedemo"> three.js demo </Link>
      </li>
      <li>
        <nav>
          <li>
            <Link to="/dashboard/victorydemo"> Victory Static Demo</Link>
          </li>
          <li>
            <Link to="/dashboard/lineplot"> Line plot with live data</Link>
          </li>
          <li>
            <Link to="/dashboard/vectorplot"> Vector plot with live data</Link>
          </li>
          <li>
            <Link to="/dashboard/phonedemo"> Phone Live Data Demo</Link>
          </li>
          <li>
            <Link to="/dashboard/madgwick"> Madgwick Filter Demo</Link>
          </li>
        </nav>
      </li>
    </nav>
  );
};

export default SideBar;
