import React from "react";
import ReactDOM from "react-dom";
import { Layout } from "./components";
import { reportWebVitals } from "./services";
import { createBrowserHistory, History } from "history";
import { Router, Route } from "react-router-dom";

const rootElement = document.getElementById("root");
const customHistory = createBrowserHistory({});

declare global {
  interface Window {
    appHistory: History<unknown>;
  }
}

ReactDOM.render(
  <Router history={customHistory}>
    <Route
      component={({ hist = customHistory }) => {
        window.appHistory = hist;
        return <Layout />;
      }}
    ></Route>
  </Router>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
