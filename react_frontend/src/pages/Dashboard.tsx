import { Controller, TimeSeriesPlot, DashboardNavigation } from "../components";

const names = ["x", "y", "z"];

export function Dashboard() {
  return (
    <div>
      <DashboardNavigation />
      <Controller>
        <TimeSeriesPlot title={"test"} names={names} />
      </Controller>
    </div>
  );
}
