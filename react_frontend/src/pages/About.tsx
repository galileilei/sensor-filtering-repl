import React from "react";

interface MainProp {
  children: React.ReactChild | React.ReactChild[];
}

function Main(props: MainProp) {
  return <React.Fragment> {props.children} </React.Fragment>;
}

interface HeadingProp {
  title: string;
}

function Heading(props: HeadingProp) {
  return (
    <div>
      <h2>{props.title}</h2>
      <p> placeholder to test various things</p>
    </div>
  );
}

function About() {
  return (
    <Main>
      {[...Array(3)].map((e, i) => (
        <Heading title={i.toString()} />
      ))}
    </Main>
  );
}

export default About;
