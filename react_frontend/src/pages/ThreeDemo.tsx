import { Canvas, MeshProps, useFrame } from "react-three-fiber";
import Axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { Vector3, ArrowHelper, Mesh } from "three";
import "./style.css";
import { QueryClient, useQuery, QueryClientProvider } from "react-query";
import axios from "axios";

const queryClient = new QueryClient();

function Content() {
  return (
    <React.Fragment>
      <Box
        position={[-1.2, 0, 0]}
        onClick={() => window.appHistory.push("/todo")}
      />
      <Box
        position={[1.2, 0, 0]}
        onClick={() => window.appHistory.push("/todo")}
      />
      <Arrow
        goLive
        refreshInterval={200}
        endpoint={"/api/madgwick/latest/rotation_vector"}
      />
    </React.Fragment>
  );
}

interface Payload {
  timestamp: number;
  payload: number[];
}

interface ArrowHelperProps {
  goLive: boolean;
  refreshInterval: number;
  endpoint: string;
}

function useInterval(callback: Function, delay: number) {
  const savedCallback = useRef<Function>(() => {});

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    const id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}

function Arrow(props: ArrowHelperProps) {
  const mesh = useRef<ArrowHelper>();
  const [dir, setDir] = useState(new Vector3(1, 0, 0));
  const origin = new Vector3(0, 0, 0);
  const [data, setData] = useState<Payload[]>([
    { timestamp: -1, payload: [0, 0, 0] },
  ]);
  const { goLive, refreshInterval, endpoint } = props;

  useInterval(() => {
    function fetchData(endpoint: string) {
      Axios.get(endpoint)
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => console.log(err));
    }
    fetchData(endpoint);
    const angle = data[data.length - 1].payload;
    setDir(new Vector3(...angle));
  }, refreshInterval);
  // update the result in direction

  useFrame(() => {
    if (mesh && mesh.current) mesh.current.setDirection(dir);
  });

  return <arrowHelper args={[dir, origin]} />;
}

function Box(props: MeshProps) {
  const mesh = useRef<Mesh>(null);

  const [hovered, setHover] = useState(false);
  const [active, setActive] = useState(false);

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => {
    mesh!.current!.rotation.x = mesh!.current!.rotation.y += 0.01;
  });

  return (
    <mesh
      {...props}
      ref={mesh}
      scale={active ? [1.5, 1.5, 1.5] : [1, 1, 1]}
      onClick={(event) => setActive(!active)}
      onPointerOver={(event) => setHover(true)}
      onPointerOut={(event) => setHover(false)}
    >
      <boxBufferGeometry args={[1, 1, 1]} />
      <meshStandardMaterial
        attach="material"
        color={hovered ? "hotpink" : "orange"}
      />
    </mesh>
  );
}

function Lights() {
  return (
    <>
      <pointLight intensity={0.3} />
      <ambientLight intensity={2} />
    </>
  );
}

function ThreeDemo() {
  return (
    <Canvas>
      <Lights />
      <Content />
    </Canvas>
  );
}

export default ThreeDemo;
