import React from "react";

function Home() {
  return (
    <div>
      <h2> Home</h2>
      <p>a placeholder to test out various react components</p>
    </div>
  );
}

export default Home;
