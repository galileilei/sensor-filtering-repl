import {useRef, useEffect} from "react";

export function useInterval(callback: Function, delay?: number): void {
  const savedCallback = useRef<Function>(() => {});

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    console.log(`delay is ${delay}`)
    if (delay !== null) {
    const id = setInterval(tick, delay);
    return () => clearInterval(id);
    }
  }, [delay]);
}