import Axios from "axios";
import {useInterval} from "./useInterval";
import {useEffect, useState} from "react";
//import { useElapsedTime } from "./useElapsedTime";

/**
 * Repeatedly calling the same endpoint to fetch a list of data.
 * @param initialData data to initialize from
 * @param endpoint backend API endpoint
 * @param refreshMs parameter to useInterval()
 * 
 * returns the data fetched.
 */
export function useQuery<T>(initialData: T, endpoint: string, refreshMs?: number) {
  const [data, setData] = useState<T>(initialData);

  useInterval(() => {
    Axios.get(endpoint)
    .then((res) => {if (res.data.length > 0) setData(res.data);})
    .catch((err) => {console.log(err)})
  }, refreshMs)

  return data;
}

// see ../../fastapi_backend/schema
export type Pair = [x:number, y:number]
export type Vector = [x:number, y:number, z:number]
export type Quaternion = [x:number, y:number, z:number, w:number]


// uglist code I have written, and it might not even work
// Edit: It works.
export interface SensorPayload {
  gps_location: Pair
  accelerometer: Vector
  gyroscope: Vector
  magnetic_field: Vector
  orientation: Vector
  linear_acceleration: Vector
  gravity: Vector
  rotation_vector: Vector
  pressure: number
  battery_temperature: number
  sender: string
  event_timestamp: number
  // filter output
  quat1: Quaternion
  euler1: Vector
  quat2: Quaternion
  euler2: Vector
  accelerometer_direction: Vector
  measured_heading: Vector
  // placeholder for dummy input
  placeholder: number
}

function getDefaultSensorPayload() : SensorPayload {
  const zeroPair : Pair = [0, 0]
  const zeroVector : Vector = [0, 0, 0]
  const unitDirection : Vector = [1, 0, 0]
  const unitQuaternion: Quaternion = [0, 0, 0, 1]

  return {gps_location :zeroPair,
  accelerometer: zeroVector,
  gyroscope: zeroVector,
  magnetic_field: zeroVector,
  orientation: zeroVector,
  linear_acceleration: zeroVector,
  gravity: zeroVector,
  rotation_vector: zeroVector,
  pressure: 0,
  battery_temperature: 0,
  sender: 'localhost',
  event_timestamp: 0,
  quat1: unitQuaternion,
  euler1: zeroVector,
  quat2: unitQuaternion,
  euler2: zeroVector,
  accelerometer_direction: zeroVector,
  measured_heading: zeroVector,
  placeholder: 0}
}

export const defaultSensorPayload = [getDefaultSensorPayload()];

/**
 * Repeated fetch the latest batch of payload. Each batch
 * is supposed to be mutially exclusive.
 * 
 * @param refreshMs frequency of accessing {since} api endpoint
 * 
 * returns the latest batch of payload since last call. 
 */
export function useQuerySince(initEndpoint: string, refreshMs?: number) {
  const [endpoint, setEndpoint] = useState<string>(initEndpoint)
  const data = useQuery(defaultSensorPayload, endpoint, refreshMs)
  const [current, setCurrent] = useState<number>(0);

  // keeps on getting data
  useEffect(() => {
      if (data.length > 0) {
        const latest = data[data.length -1].event_timestamp;
        setCurrent(latest);
      }
      setEndpoint("http://localhost:8000/api/madgwick/since/" + initEndpoint + current.toString())
    }, [data])

  return data;
}

/**
 * Repeatedly calling useQuerySince(), and only keep payload which has not expired,
 * i.e., less than {keep} seconds from the latest payload.
 * 
 * @param refreshMs frequency of calling useQuerySince()
 * @param keep the duration of data to keep in range
 * 
 * return the list of fresh payload.
 */
export function useBuffer(refreshMs?: number, keep: number = 10, sensorType: "madgwick" | "mahony") {
  const data = useQuerySince(refreshMs)
  const [buffer, setBuffer] = useState(defaultSensorPayload)

  // not sure if this works at all
  useEffect(() => {
    if(data.length > 0) {
      var newBuffer = Array.from(buffer)
      const latest = data[data.length - 1].event_timestamp;
      for(var i = 0; i < newBuffer.length; i++) {
        const payload = newBuffer[i];
        if (payload.event_timestamp <= latest - keep ) newBuffer.shift();
        else break;
      }
      newBuffer.push(...data)
      setBuffer(newBuffer)
    }
  }, [data])

  return buffer
}