import THREE, {Group} from "three";
import {useRef} from "react";
import {useFrame} from "react-three-fiber";

// we are doing this because typeof is a Javascript keyword
// and thus only work on primitive types:
// for typescript custom type, typeof only return Object.
/*
export type EulerDir = {
  type: "Euler";
  dir: Parameters<THREE.Euler["set"]>;
};

export type QuaternionDir = {
  type: "Quaternion";
  dir: Parameters<THREE.Quaternion["set"]>;
};
*/

export function useRotation(dir: number[]) : React.MutableRefObject<Group> {
    const grp = useRef<Group>();

    useFrame(() => {
        if (grp && grp.current) {
          switch (dir.length) {
            case 3:
              console.log(`euler is ${JSON.stringify(dir)}`)
              //@ts-ignore
              grp.current.rotation.set(...dir);
              return;
            case 4:
              console.log(`quaternion is ${JSON.stringify(dir)}`)
              //@ts-ignore
              grp.current.quaternion.set(...dir);
              return;
          }
        }
      });

    return grp;
}