import Todo from "./_Todo";
import { DashboardNavigation } from "../../components";

export default function Dashboard() {
  return (
    <div>
      <DashboardNavigation />
      <p> Dashboard page</p>
    </div>
  );
}
