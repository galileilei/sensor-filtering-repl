import dynamic from "next/dynamic";

const DynamicComponent = dynamic(
  () =>
    import("../../components/MadgwickDemo").then(
      (mod) => mod.MadgwickDashboard
    ),
  { ssr: false }
);

export default function MadgwickDemo() {
  return <DynamicComponent />;
}
