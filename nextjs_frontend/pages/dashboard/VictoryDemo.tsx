import * as React from "react";
import {
  VictoryChart,
  VictoryLine,
  VictoryScatter,
  InterpolationPropType,
  VictoryAxis,
  VictoryTheme,
} from "victory";

import { GridList, GridListTile, Card } from "@material-ui/core";

const data = [
  { x: 0, y: 0 },
  { x: 1, y: 2 },
  { x: 2, y: 1 },
  { x: 3, y: 4 },
  { x: 4, y: 3 },
  { x: 5, y: 5 },
];

const cartesianInterpolations: Array<InterpolationPropType> = [
  "basis",
  "bundle",
  "cardinal",
  "catmullRom",
  "linear",
  "monotoneX",
  "monotoneY",
  "natural",
  "step",
  "stepAfter",
  "stepBefore",
];

const polarInterpolations: Array<InterpolationPropType> = [
  "basis",
  "cardinal",
  "catmullRom",
  "linear",
];

interface InterpolationSelectProp {
  value: InterpolationPropType;
  values: Array<InterpolationPropType>;
  setValue: React.Dispatch<React.SetStateAction<InterpolationPropType>>;
}

function InterpolationSelect({
  value,
  values,
  setValue,
}: InterpolationSelectProp) {
  return (
    <select
      onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
        setValue(event.target.value as InterpolationPropType);
      }}
      value={value}
      style={{ width: 75 }}
    >
      {values.map((value) => (
        <option value={value} key={value}>
          {value}
        </option>
      ))}
    </select>
  );
}

/* change the following to functional components */

function StaticChart() {
  const [
    interpolation,
    setInterpolation,
  ] = React.useState<InterpolationPropType>("linear");

  const [polar, togglePolar] = React.useState<boolean>(false);

  return (
    <div className="victorychart">
      <h1> Placeholder </h1>
      <InterpolationSelect
        value={interpolation}
        values={polar ? polarInterpolations : cartesianInterpolations}
        setValue={setInterpolation}
      />
      <input
        type="checkbox"
        id="polar"
        value={polar ? 1 : 0}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          togglePolar(!polar);
          setInterpolation("linear");
        }}
        style={{ marginLeft: 25, marginRight: 5 }}
      />
      <label htmlFor="polar">polar</label>
      <VictoryChart
        polar={polar}
        height={200}
        width={300}
        theme={VictoryTheme.material}
      >
        <VictoryLine
          interpolation={interpolation}
          data={data}
          style={{ data: { stroke: "#c43a31" } }}
        />
        <VictoryScatter
          data={data}
          size={5}
          style={{ data: { fill: "#c43a31" } }}
        />
        <VictoryAxis crossAxis />
        <VictoryAxis dependentAxis crossAxis />
      </VictoryChart>
    </div>
  );
}

function GridWrap(elem: JSX.Element) {
  return (
    <GridListTile cols={1} rows={1}>
      <Card variant="elevation" color="secondary">
        {elem}
      </Card>
    </GridListTile>
  );
}

export default function VictoryDemo() {
  return (
    <GridList cellHeight={600} spacing={25}>
      {[1, 2, 3, 4, 5, 6].map((e) => GridWrap(StaticChart()))}
    </GridList>
  );
}
