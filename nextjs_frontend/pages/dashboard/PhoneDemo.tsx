import React from "react";
import { VectorPlot } from "../../components";

const defaultNames: [string, string, string] = ["x", "y", "z"];

function Sensor(prop: Prop) {
  function getSensor(dtype: string = prop.dtype) {
    function getEndpoint(dtype: string) {
      const prefix = "http://localhost:8000/api/phone/latest"; // next.js can forward the request properly
      return prefix + "/" + dtype;
    }
    return (
      <VectorPlot
        names={defaultNames}
        title={dtype}
        endpoint={getEndpoint(dtype)}
        refreshInUrl={200}
      />
    );
  }

  return getSensor(prop.dtype);
}

interface Prop {
  dtype: string;
}

function PhoneDemo() {
  return (
    <div className="twocols">
      <div className="leftcol">
        <Sensor dtype="accelerometer" />
        <Sensor dtype="gyroscope" />
        <Sensor dtype="magnetic_field" />
      </div>
      <div className="rightcol">
        <Sensor dtype="orientation" />
        <Sensor dtype="euler1" />
      </div>
    </div>
  );
}

export default PhoneDemo;
