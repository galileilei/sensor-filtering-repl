import React from "react";
import { TimeSeriesPlot } from "../../components";
import { GridList, GridListTile, GridListTileBar } from "@material-ui/core";
import { Slider } from "../../components";

interface LiveSensorProp {
  dtype: string;
  names: string[];
  refreshInUrl: number;
  goLive: boolean;
}

function LiveSensor(props: LiveSensorProp) {
  const { goLive, refreshInUrl, names, dtype: dtype } = props;
  const api_port = "http://localhost:8000/api/madgwick/latest/" + dtype;
  return (
    <GridListTile cols={1} rows={1}>
      <TimeSeriesPlot
        title={dtype}
        names={names}
        endpoint={api_port}
        refreshMs={refreshInUrl}
        goLive={goLive}
      />
      <GridListTileBar title={dtype} titlePosition="top" />
    </GridListTile>
  );
}

export default function MadgwickDemo() {
  const [goLive, toggleGoLive] = React.useState<boolean>(true);
  const [refreshInterval, setRefreshInterval] = React.useState<number>(200);

  return (
    <div>
      <Slider
        goLive={goLive}
        toggleGoLive={toggleGoLive}
        refreshInterval={refreshInterval}
        setRefreshInterval={setRefreshInterval}
      />
      <GridList cellHeight={600} spacing={25} cols={2}>
        <LiveSensor
          dtype="accelerometer"
          names={["x", "y", "z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
        <LiveSensor
          dtype="gyroscope"
          names={["x", "y", "z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
        <LiveSensor
          dtype="magnetic_field"
          names={["x", "y", "z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
        <LiveSensor
          dtype="rotation_vector"
          names={["about x", "about y", "about z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
        <LiveSensor
          dtype="euler1"
          names={["about x", "about y", "about z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
        <LiveSensor
          dtype="quaternion"
          names={["v", "x", "y", "z"]}
          refreshInUrl={refreshInterval}
          goLive={goLive}
        />
      </GridList>
    </div>
  );
}
