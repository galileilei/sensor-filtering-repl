import Todo from "./_Todo";
import React from "react";
import { MainNavigation } from "../../components";

export default function Landing() {
  return (
    <div>
      <MainNavigation />
      <Todo />
    </div>
  );
}
