import * as React from "react";
import { AppProps } from "next/app";
import theme from "../src/theme";
import { ThemeProvider } from "theme-ui";
import { Layout } from "../components";

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  );
};

export default MyApp;
