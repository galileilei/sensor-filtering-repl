import { Canvas, MeshProps, useFrame } from "react-three-fiber";
import Axios from "axios";
import React, { useRef, useState } from "react";
import { useInterval } from "../../hooks";
import { Vector3, ArrowHelper, Mesh } from "three";
import { Box } from "theme-ui";

function Content() {
  return (
    <React.Fragment>
      <ThreeBox position={[-1.2, 0, 0]} />
      <ThreeBox position={[1.2, 0, 0]} />
      <Arrow
        goLive
        refreshInterval={200}
        endpoint={"http://localhost:8000/api/madgwick/latest/rotation_vector"}
      />
    </React.Fragment>
  );
}

interface Payload {
  timestamp: number;
  payload: number[];
}

interface ArrowHelperProps {
  goLive: boolean;
  refreshInterval: number;
  endpoint: string;
}

function Arrow(props: ArrowHelperProps) {
  const mesh = useRef<ArrowHelper>();
  const [dir, setDir] = useState(new Vector3(1, 0, 0));
  const origin = new Vector3(0, 0, 0);
  const [data, setData] = useState<Payload[]>([
    { timestamp: -1, payload: [0, 0, 0] },
  ]);
  const { goLive, refreshInterval, endpoint } = props;

  useInterval(() => {
    function fetchData(endpoint: string) {
      Axios.get(endpoint)
        .then((res) => {
          if (res.data.length > 1) setData(res.data);
        })
        .catch((err) => console.log(err));
    }

    fetchData(endpoint);
    if (data.length > 1) {
      const angle = data[data.length - 1].payload;
      setDir(new Vector3(...angle));
    }
  }, refreshInterval);
  // update the result in direction

  useFrame(() => {
    if (mesh && mesh.current) mesh.current.setDirection(dir);
  });

  return <arrowHelper args={[dir, origin]} />;
}

function ThreeBox(props: MeshProps) {
  const mesh = useRef<Mesh>();

  const [hovered, setHover] = useState(false);
  const [active, setActive] = useState(false);

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => {
    mesh.current.rotation.x = mesh.current.rotation.y += 0.01;
  });

  return (
    <mesh
      {...props}
      ref={mesh}
      scale={active ? [1.5, 1.5, 1.5] : [1, 1, 1]}
      onClick={(event) => setActive(!active)}
      onPointerOver={(event) => setHover(true)}
      onPointerOut={(event) => setHover(false)}
    >
      <boxBufferGeometry args={[1, 1, 1]} />
      <meshStandardMaterial
        attach="material"
        color={hovered ? "hotpink" : "orange"}
      />
    </mesh>
  );
}

function Lights() {
  return (
    <>
      <pointLight intensity={0.3} />
      <ambientLight intensity={2} />
    </>
  );
}

function ThreeDemo() {
  return (
    <Box width={1000} height={1000}>
      <Canvas>
        <Lights />
        <Content />
      </Canvas>
    </Box>
  );
}

export default ThreeDemo;
