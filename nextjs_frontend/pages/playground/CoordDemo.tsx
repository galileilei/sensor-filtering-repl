import { Canvas } from "react-three-fiber";
import * as React from "react";
import { CustomAxes, RotatingArrow } from "../../components";
import { Box } from "theme-ui";

interface ArrowProp {
  speed: number;
}

// Emulate ArrowHelper, but does not recreate
// props on each re-render

function Lights() {
  return (
    <>
      <pointLight intensity={0.3} />
      <ambientLight intensity={2} />
    </>
  );
}

function Content() {
  return (
    <>
      <CustomAxes />
      <RotatingArrow speed={0.01} />
      <RotatingArrow speed={-0.02} />
    </>
  );
}

export default function CoordDemo() {
  return (
    <Box>
      <Canvas camera={{ position: [10, 10, 10] }}>
        <Lights />
        <Content />
      </Canvas>
    </Box>
  );
}
