import * as React from "react";
import {
  Controller,
  Plotter,
  OrientationPlotter,
  MakeLegend,
  LegendProp,
} from "../../components";

import { Box, Flex } from "theme-ui";

interface LiveSensorProp {
  dtype: string;
  names?: string[];
}

function LiveSensor({ dtype, names = axesNames }: LiveSensorProp) {
  return (
    <Box>
      <Plotter dtype={dtype} names={names} />
    </Box>
  );
}

const legendA: LegendProp[] = [
  { color: "#d9dab0", name: "north_pole" },
  { color: "#ec4646", name: "accelerometer_direction" },
  { color: "#51c2d5", name: "measured_heading" },
];

// TODO: check three.js quaternion format, if zero vector is (1, 0, 0, 0) or (0, 0, 0, 1)
const legendB: LegendProp[] = [
  { color: "#000000", name: "rotation_vector" },
  { color: "#ec4646", name: "euler1" },
  { color: "#51c2d5", name: "euler2" },
  //{ color: "#76c200", name: "quant1" },
  //{ color: "#c27601", name: "quant2" },
];

const axesNames = ["x", "y", "z"];

export default function ControllerDemo() {
  return (
    <Controller>
      <Flex sx={{ flexDirection: "column", minHeight: "100vh" }}>
        {/*
          <LiveSensor dtype="accelerometer" />
          <LiveSensor dtype="magnetic_field" />
          <LiveSensor dtype="euler1" />
          <LiveSensor dtype="rotation_vector" />
          <LiveSensor dtype="euler2" />
          */}
        <MakeLegend legends={[...legendA, ...legendB]} />
        <OrientationPlotter legendA={legendA} legendB={legendB} />
      </Flex>
    </Controller>
  );
}
