import Axios from "axios";
import React from "react";
import { VictoryChart, VictoryLine, VictoryScatter } from "victory";

const refreshInUrl = 100;

interface Pair {
  x: number;
  y: number;
}

type Payload = Pair[];

function LinePlot() {
  const initialData: Payload = [
    { x: 0, y: 0 },
    { x: 1, y: 2 },
    { x: 2, y: 1 },
    { x: 3, y: 4 },
    { x: 4, y: 3 },
    { x: 5, y: 5 },
  ];
  const [data, setData] = React.useState<Payload>(initialData);
  const [refreshInterval] = React.useState(refreshInUrl);

  React.useEffect(() => {
    const fetchMetrics = () => {
      Axios.get<Payload>("http://localhost:8000/api/mock/latest")
        .then((res) => setData(res.data as Payload))
        .catch((err) => console.log(err));
    };

    fetchMetrics();
    if (refreshInterval && refreshInterval > 0) {
      const interval = setInterval(fetchMetrics, refreshInterval);
      return () => clearInterval(interval);
    }
  }, [refreshInterval]);

  return (
    <div>
      <VictoryChart polar={false} height={390}>
        <VictoryLine
          interpolation="catmullRom"
          data={data}
          style={{ data: { stroke: "#c43a31" } }}
        />
        <VictoryScatter
          data={data}
          size={2}
          style={{ data: { fill: "#c43a31" } }}
        />
      </VictoryChart>
    </div>
  );
}

export default LinePlot;
