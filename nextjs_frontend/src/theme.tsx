import { polaris } from "@theme-ui/presets";

const theme = {
  ...polaris,
  style: {
    ...polaris.styles,
  },
};

export default theme;
