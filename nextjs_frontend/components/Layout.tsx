/** @jsxRuntime classic */
/** @jsx jsx */
import * as React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Nav from "./navBar";
import { Box, Flex, jsx } from "theme-ui";

function Footer() {
  return <div>testing</div>;
}

export function Layout({ children }) {
  const { pathname } = useRouter();
  return (
    <React.Fragment>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title key="title"> Sensor REPL</title>
        <meta name="description" content="fastAPI + Nextjs" />
      </Head>

      <Flex sx={{ flexDirection: "column", minHeight: "100vh" }}>
        <Nav />
        <Box id="main-content">{children}</Box>
        {/*}
        {/*<Footer />*/}
      </Flex>
    </React.Fragment>
  );
}
