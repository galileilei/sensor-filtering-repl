import { Flex, Box } from "theme-ui";
import { useRouter } from "next/router";
import Link from "next/link";
import * as React from "react";

interface NavLinkProps {
  href: string;
  key: string;
}

const MainNav: NavLinkProps[] = [
  { href: "/about", key: "About" },
  { href: "/default", key: "Default" },
  { href: "/todo", key: "Todo App" },
  { href: "/dashboard", key: "Dashboard" },
  { href: "/playground", key: "Playground" },
];

const DashboardNav: NavLinkProps[] = [
  { href: "/VictoryDemo", key: "Static" },
  { href: "/PhoneDemo", key: "Phone" },
  { href: "/MadgwickDemo", key: "Madgwick" },
];

const PlaygroundNav: NavLinkProps[] = [
  { href: "/ThreejsDemo", key: "Three.js" },
  { href: "/LinePlot", key: "Victory.js Line Plot" },
  { href: "/CoordDemo", key: "Coordinate System" },
  { href: "/ControllerDemo", key: "Controller + Plotter Demo" },
];

function Navigation(prefix: string = "", props: NavLinkProps[]) {
  return (
    <Flex bg="muted" as="nav">
      {props.map((e) => (
        <Box m={4} key={e.key}>
          <Link href={prefix + e.href} passHref>
            {e.key}
          </Link>
        </Box>
      ))}
    </Flex>
  );
}

const Nav: React.FC = () => {
  const { pathname } = useRouter();
  if (pathname.includes("/dashboard"))
    return Navigation("/dashboard", DashboardNav);
  if (pathname.includes("/playground"))
    return Navigation("/playground", PlaygroundNav);
  return Navigation("", MainNav);
};

export default Nav;
