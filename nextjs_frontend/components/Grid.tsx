import { GridList, GridListTile, GridListTileBar } from "@material-ui/core";
import { WithStyles } from "@material-ui/core/styles";
import clsx from "clsx";

interface Props extends WithStyles<{}> {
  children?: React.ReactNode;
  className?: string;
}

export function MyGridList({ children, classes, className, ...other }) {
  return (
    <GridList className={clsx(classes.root, className)} {...other}>
      {" "}
      {children}{" "}
    </GridList>
  );
}

export function MyGridListTile(props) {
  return <GridListTile {...props} />;
}

export function MyGridListTitleBar(props) {
  return <GridListTileBar {...props} />;
}
