import { Mesh, Group } from "three";
import { Canvas, useFrame } from "react-three-fiber";
import { Box } from "theme-ui";
import React, { useContext, useEffect, useRef } from "react";
import { DataContext } from "./Controller";
import { useRotation } from "../hooks";
import { LegendProp } from "./icons";

interface ArrowProp {
  key: string;
  color: string;
  dir?: number[];
  position?: [x: number, y: number, z: number];
}

const vertices = new Float32Array([0, 0, 0, 0.8, 0, 0]);

function Arrow({
  key,
  color,
  dir = [0, 0, 0],
  position = [0, 0, 0],
}: ArrowProp) {
  const cone = React.useRef<Mesh>();
  const grp = useRotation(dir);

  // run on construction only, hence no dependency
  useEffect(() => {
    cone.current.geometry.rotateZ(-0.5 * Math.PI);
    cone.current.geometry.translate(0.8, 0, 0);
  }, []);

  return (
    <group ref={grp} position={position}>
      <line>
        <bufferGeometry>
          <bufferAttribute
            attachObject={["attributes", "position"]}
            itemSize={3}
            array={vertices}
            count={2}
          />
        </bufferGeometry>
        <lineBasicMaterial attach="material" color={color} linewidth={10} />
      </line>
      <mesh ref={cone}>
        <cylinderBufferGeometry
          attach="geometry"
          args={[0, 0.5 * 0.2, 0.2, 5, 1]}
        ></cylinderBufferGeometry>
        <meshStandardMaterial
          attach="material"
          color="black"
          toneMapped={false}
        />
      </mesh>
    </group>
  );
}

// Testing if Arrow implmentation is alright
export function CustomAxes() {
  return (
    <group>
      <Arrow color="#00000" key="x" />
      <Arrow color="#00000" key="y" dir={[0, 0, 0.5 * Math.PI]} />
      <Arrow color="#00000" key="z" dir={[0, 0.5 * Math.PI, 0]} />
    </group>
  );
}

interface OrientationPlotterProp {
  legendA: LegendProp[];
  legendB: LegendProp[];
}

/**
 * shows a set of directions as arrows with origin @ (-1, 0, 0),
 * and another set of directions as arrows with origin @ (1, 0, 0).
 * @param props
 */
export function OrientationPlotter({
  legendA,
  legendB,
}: OrientationPlotterProp) {
  const bundle = useContext(DataContext);
  const latest = bundle[bundle.length - 1];

  return (
    <Box sx={{ width: 600, height: 600, flex: "0 1 auto" }}>
      <ThreejsScene>
        {legendA.map(({ name, color }) => (
          <Arrow
            key={name} // needed by react to keep track
            color={color}
            dir={latest[name]}
            position={[-1, 0, 0]}
          />
        ))}
        {<CustomAxes />}
        {<RotatingArrow speed={0.01} />}
        {legendB.map(({ name, color }) => (
          <Arrow
            key={name} // needed by react to keep track
            color={color}
            dir={latest[name]}
            position={[1, 0, 0]}
          />
        ))}
      </ThreejsScene>
    </Box>
  );
}

function ThreejsScene({ children }) {
  return (
    <Canvas camera={{ position: [2, 2, 2] }}>
      <pointLight intensity={0.3} />
      <ambientLight intensity={2} />
      <CustomAxes />
      {children as React.ReactNode}
    </Canvas>
  );
}

export function RotatingArrow({ speed }) {
  const cone = React.useRef<Mesh>();
  const grp = React.useRef<Group>();

  const vertices = new Float32Array([0, 0, 0, 0.8, 0, 0]);

  useEffect(() => {
    cone.current.geometry.rotateZ(-0.5 * Math.PI);
    cone.current.geometry.translate(0.8, 0, 0);
  }, []);

  useFrame(() => {
    if (grp && grp.current) {
      grp.current.rotation.x = grp.current.rotation.y += speed;
    }
  });

  return (
    <group ref={grp}>
      <line>
        <bufferGeometry>
          <bufferAttribute
            attachObject={["attributes", "position"]}
            itemSize={3}
            array={vertices}
            count={2}
          />
        </bufferGeometry>
        <lineBasicMaterial attach="material" color="red" linewidth={10} />
      </line>
      <mesh ref={cone}>
        <cylinderBufferGeometry
          attach="geometry"
          args={[0, 0.5 * 0.2, 0.2, 5, 1]}
        ></cylinderBufferGeometry>
        <meshStandardMaterial
          attach="material"
          color="black"
          toneMapped={false}
        />
      </mesh>
    </group>
  );
}
