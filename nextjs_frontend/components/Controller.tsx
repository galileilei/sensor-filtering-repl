import { useQuerySince, defaultSensorPayload, useBuffer } from "../hooks";
import React, { createContext } from "react";
import { Container } from "theme-ui";

export const DataContext = createContext(defaultSensorPayload);

interface SliderProp {
  goLive: boolean;
  toggleGoLive: React.Dispatch<React.SetStateAction<boolean>>;
  refreshInterval: number;
  setRefreshInterval: React.Dispatch<React.SetStateAction<number>>;
}

export function Slider(props: SliderProp) {
  return (
    <Container p={4} bg="muted">
      <input
        type="checkbox"
        id="golive"
        value={props.goLive ? 1 : 0}
        onChange={(event) => {
          props.toggleGoLive((x) => !x);
        }}
      />
      <label htmlFor="golive"> Go Live Now! </label>
      <input
        type="range"
        id="refreshrate"
        min={25}
        max={1000}
        value={props.refreshInterval}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          console.log(`value is ${event.target.value}`);
          props.setRefreshInterval(+event.target.value);
        }}
      ></input>
      <label htmlFor="refreshrate">
        {" "}
        refresh rate = {props.refreshInterval} ms{" "}
      </label>
    </Container>
  );
}

export function Controller({ children }) {
  // fetch the data in useQuerySince
  // load the data in context
  const [goLive, toggleGoLive] = React.useState<boolean>(false);
  const [refreshInterval, setRefreshInterval] = React.useState<number>(200);
  const data = useBuffer(goLive ? refreshInterval : null, 5);

  return (
    <div>
      <Slider
        goLive={goLive}
        toggleGoLive={toggleGoLive}
        refreshInterval={refreshInterval}
        setRefreshInterval={setRefreshInterval}
      />
      <DataContext.Provider value={data}>
        {children as React.ReactNode}
      </DataContext.Provider>
    </div>
  );
}
