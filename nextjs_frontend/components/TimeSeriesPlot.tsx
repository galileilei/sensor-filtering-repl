import Axios from "axios";
import React, { useLayoutEffect, useState } from "react";
import {
  VictoryChart,
  VictoryLegend,
  VictoryLine,
  VictoryScatter,
  VictoryTheme,
  VictoryAxis,
} from "victory";
import { useInterval } from "../hooks";

interface TimeSeriesProp {
  title: string;
  names: string[];
  endpoint: string;
  refreshMs: number;
  goLive: boolean;
  size?: { height?: number; width?: number };
  domainXInit?: [number, number];
  domainYInit?: [number, number];
}

export interface Payload {
  timestamp: number;
  payload: number[];
}

export function SingleLine(index: number = 0, color: string, data?: Payload[]) {
  console.log(`index is ${index}, color is ${color}`);
  return [
    <VictoryLine
      interpolation="catmullRom"
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      style={{ data: { stroke: color } }}
    />,
    <VictoryScatter
      data={data}
      x={"timestamp"}
      y={["payload", index.toString()]}
      size={5}
      style={{ data: { fill: color } }}
    />,
  ];
}

export function defaultPointStyle(size: number = 3) {
  const defaultColors = ["#BC243C", "#5B5EA6", "#88B04B"];
  var colors = defaultColors;
  if (size == 4) {
    colors.push("#F99157");
  }
  const pointType: string[] = Array(size).fill("circle");
  const pointStyle = colors.map(function (e, i) {
    return { fill: e, type: pointType[i] };
  });
  return { colors: colors, pointStyle: pointStyle };
}

export function TimeSeriesPlot(props: TimeSeriesProp) {
  const {
    title,
    names,
    endpoint,
    refreshMs,
    goLive,
    size = {},
    domainXInit = [-10, 10],
    domainYInit = [-10, 10],
  } = props;
  const { height = 300, width = 400 } = size;
  const [data, setData] = useState<Payload[]>([
    {
      timestamp: -1,
      payload: [0, 0, 0],
    },
  ]);

  const { colors, pointStyle } = defaultPointStyle();

  const [domainXRange, setDomainXRange] = useState(domainXInit);
  const [domainYRange, setDomainYRange] = useState(domainYInit);
  const multi = props.names.length;

  useInterval(() => {
    Axios.get<Payload[]>(endpoint)
      .then((res) => setData(res.data as Payload[]))
      .catch((err) => console.log(err));
  }, refreshMs);

  // still bugged
  useLayoutEffect(() => {
    if (typeof data === "undefined") return;
    const xvals = data.map((e) => e.timestamp);
    const yvals = data.map((e) => e.payload).flat();
    const xmin = Math.min(...xvals);
    const ymin = Math.min(...yvals);
    const xmax = Math.max(...xvals);
    const ymax = Math.max(...yvals);
    const xrange = xmax - xmin;
    const yrange = ymax - ymin;
    setDomainXRange([xmin - xrange * 0.1, xmax + xrange * 0.1]);
    setDomainYRange([ymin - yrange * 0.1, ymax + yrange * 0.1]);
  }, [data]);

  return (
    <div>
      <h1
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {props.title}
      </h1>
      <br />

      <VictoryChart
        polar={false}
        theme={VictoryTheme.material}
        height={height}
        width={width}
      >
        <VictoryAxis
          crossAxis
          width={width}
          height={height}
          domain={domainXRange}
        />
        <VictoryAxis
          dependentAxis
          crossAxis
          width={width}
          height={height}
          domain={domainYRange}
        />
        {/* various lines */}
        {[Array(multi)]
          .map((e, i) => SingleLine(i, colors[i] as string, data))
          .flat()}
        {/* the legend in the top right corner */}
        <VictoryLegend
          x={width * 0.9}
          y={height * 0.1}
          title="Legend"
          centerTitle
          orientation="vertical"
          gutter={20}
          data={[...Array(multi)].map((e, i) => ({
            name: names[i],
            symbol: pointStyle[i],
          }))}
        />
      </VictoryChart>
    </div>
  );
}
