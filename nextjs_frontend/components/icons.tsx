import * as React from "react";
import { Flex, Box } from "theme-ui";
interface IconProp {
  size: number;
  color: string;
  stroke?: number;
}

function Icon({ size, color, stroke = 3 }: IconProp) {
  return (
    <svg width={size + stroke} height={size + stroke}>
      <rect
        width={size}
        height={size}
        style={{ fill: color, strokeWidth: stroke, stroke: "#000000" }}
      />
    </svg>
  );
}

export interface LegendProp {
  name: string;
  color: string;
  size?: number;
  stroke?: number;
}

function Legend({ name, color, size = 10, stroke = 1 }: LegendProp) {
  return (
    <Flex>
      <Box>
        <Icon size={size} color={color} stroke={stroke} />
      </Box>
      <Box>{name}</Box>
    </Flex>
  );
}

export function MakeLegend({ legends }) {
  return (
    <Flex sx={{ flexWrap: "wrap", justifyContent: "space-evenly" }}>
      {(legends as LegendProp[]).map(({ name, color }) => (
        <Box sx={{ flex: "1 1 auto" }}>
          <Legend key={name} name={name} color={color} />
        </Box>
      ))}
    </Flex>
  );
}
