import { useContext, useEffect, useState } from "react";
import { DataContext } from "./Controller";
import { SingleLine, defaultPointStyle, Payload } from "./TimeSeriesPlot";
import {
  VictoryChart,
  VictoryLegend,
  VictoryTheme,
  VictoryAxis,
} from "victory";
import { SensorPayload } from "../hooks";
import { Box } from "theme-ui";

interface PlotterProp {
  dtype: string;
  names: string[];
  size?: { height?: number; width?: number };
  domainXInit?: [number, number];
  domainYInit?: [number, number];
}

function extractPayload(bundle: SensorPayload[], dtype: string): Payload[] {
  return bundle.map((e) => ({
    timestamp: e.event_timestamp,
    payload: e[dtype],
  }));
}

export function Plotter(props: PlotterProp) {
  const {
    dtype,
    names,
    size = {},
    domainXInit = [-10, 10],
    domainYInit = [-10, 10],
  } = props;
  const { height = 300, width = 400 } = size;

  const [domainXRange, setDomainXRange] = useState(domainXInit);
  const [domainYRange, setDomainYRange] = useState(domainYInit);
  // TODO: only load it once
  const TimeSeriesStyle = defaultPointStyle(names.length);

  // get data from context
  const bundle = useContext(DataContext);
  //console.log(`bundle is ${JSON.stringify(bundle)} in ${dtype}`);

  // update the data if bundle change
  const [data, setData] = useState<Payload[]>();
  useEffect(() => {
    //console.log(`bundle is ${data}`);
    setData(extractPayload(bundle, dtype));
    console.log(`payload is ${JSON.stringify(data)}`);
  }, [bundle]);

  // update the range
  useEffect(() => {
    if (typeof data === "undefined") return;
    const xvals = data.map((e) => e.timestamp);
    const yvals = data.map((e) => e.payload).flat();
    const xmin = Math.min(...xvals);
    const ymin = Math.min(...yvals);
    const xmax = Math.max(...xvals);
    const ymax = Math.max(...yvals);
    const xrange = xmax - xmin;
    const yrange = ymax - ymin;
    setDomainXRange([xmin - xrange * 0.1, xmax + xrange * 0.1]);
    setDomainYRange([ymin - yrange * 0.1, ymax + yrange * 0.1]);
  }, [data]);

  return (
    <Box>
      <p>{dtype}</p>
      <VictoryChart
        polar={false}
        theme={VictoryTheme.material}
        height={height}
        width={width}
      >
        <VictoryAxis
          crossAxis
          width={width}
          height={height}
          domain={domainXRange}
        />
        <VictoryAxis
          dependentAxis
          crossAxis
          width={width}
          height={height}
          domain={domainYRange}
        />
        {/* various lines */}
        {[...Array(names.length)].map((e, i) =>
          SingleLine(i, TimeSeriesStyle.colors[i] as string, data)
        )}
        {/* the legend in the top right corner */}
        <VictoryLegend
          x={width * 0.9}
          y={height * 0.1}
          title="Legend"
          centerTitle
          orientation="vertical"
          gutter={20}
          data={[...Array(names.length)].map((e, i) => ({
            name: names[i],
            symbol: TimeSeriesStyle.pointStyle[i],
          }))}
        />
      </VictoryChart>
    </Box>
  );
}
