# Introduction

An server-client implentation of REPL to test filtering algorithm. Right now it supports only mahony and madgwick. The project is inspired by the MARG video from Matlab youtube channel. see [design doc](./fastapi_backend/README.md) for how it is designed.

## How to install

The recommended way to install the server is through `poetry`. __IMPORTANT__:
install it system-wide, not with `pip` in your local virtual environment!

The recommened way to install the client is through `npm`. Google how.

Now run the following command to set up virtual environment for the server:

```bash
cd fastapi_backend/
poetry shell
poetry install
exit
```

Do the same for the client

```bash
cd nextjs_backend/
npm install
```

Only running in development mode is possible now, as the communicate between
server and client is hard-coded as `localhost`
_TODO_: put this in a docker

1. start the backend server at `localhost:8000/` in one terminal window:

```bash
cd fastapi_backend/
poetry shell
uvicorn main:app --reload --lifespan on
```

With the default config file, You should see this from the terminal:

```bash
> uvicorn main:app --reload --lifespan on
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [8804] using statreload
2021-01-22 16:06:23.142 | INFO     | main:<module>:4 - testing:./main.py, main,
INFO:     Started server process [8806]
INFO:     Waiting for application startup.
2021-01-22 16:06:23.419 | INFO     | app.core.event_handler:startup:59 - Running app start handler

2021-01-22 16:06:23.419 | DEBUG    | app.imufilter.filters:__init__:22 - Generic filter started
2021-01-22 16:06:23.420 | DEBUG    | app.imufilter.sensor:run:118 - waiting for raw data
2021-01-22 16:06:23.420 | DEBUG    | app.imufilter.sensor:__init__:114 - phone sensor started, , 5555
2021-01-22 16:06:23.420 | DEBUG    | app.imufilter.view:__init__:32 - phone view started
2021-01-22 16:06:23.421 | DEBUG    | app.imufilter.filters:__init__:22 - Generic filter started
2021-01-22 16:06:23.421 | DEBUG    | app.imufilter.filters:__init__:178 - Madgwick filter started
2021-01-22 16:06:23.421 | DEBUG    | app.imufilter.view:__init__:32 - phone view started
INFO:     Application startup complete.
```

Check if Swagger UI is running on `localhost:8000/docs`. Right now there is no sensor connected ot it, so it should return nothing.

2. start the frontend at `localhost:3000/` in another terminal window:

```bash
cd next_frontend
npm install
npm run dev
```

Check the frontend is running by checking nextjs development server is running
on `localhost:3000`.

3. We will use [android app SensorStream][sensorstream] for the backend. Use `ifconfig` (Linux) or `ipconfig` (Windows) get IP address, in order to send data. The backend filters data stream in real time and forwards the processed signal to the frontend dashboard. A few screenshots are attached to guide you:

![Put the target IP address here](./docs/sensorstream_page1.jpg)
![Check `Include User-Checked`](./docs/sensorstream_page2.jpg)

4. Now navigate to `Playground` then `Controller Demo` to view the result:

![Navigate to `Playground` then `Controller Demo`](./docs/frontend_dashboard.png)

## Contribution

Make sure you are well-versed in `Python` and the APIs of `fastAPI, pydantic, pybind11, pykka` before applying breaking changes to backend. Also you may want to understand make scripts to build madgwick and mahony libraries.

Make sure you are well-versed in `Typescript` and the APIs of `react, nextjs, victoryjs, react-three-fiber, material-ui` before applying breaking changes to frontend. Also you many want to learn React 16.0+ with hooks instead of class components.

[sensorstream]: https://play.google.com/store/apps/details?id=de.lorenz_fenster.sensorstreamgps
