from fastapi import FastAPI
from loguru import logger

logger.info(f"testing:{__file__}, {__name__}, {__package__}")

from app.api.routes.router import router
from app.core.event_handler import start_app_handler, stop_app_handler
from fastapi.middleware.cors import CORSMiddleware
from app.core.config import ALLOW_ORIGIN


def get_app() -> FastAPI:
    """
    declare everything related to FastAPI app.
    explicit and in one place, is better than implicit and scattered.
    """
    fast_app = FastAPI(title="mock", version="0.0.1", debug=True)
    fast_app.include_router(router, prefix="/api")

    fast_app.add_event_handler("startup", start_app_handler(fast_app))
    fast_app.add_event_handler("shutdown", stop_app_handler(fast_app))

    fast_app.add_middleware(
        CORSMiddleware,
        allow_origins=ALLOW_ORIGIN,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return fast_app


# to run the following command
# uvicorn main:app --reload
app = get_app()