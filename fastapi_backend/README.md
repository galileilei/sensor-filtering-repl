# Backend serving microservices using Representational State Transfer (REST)

This is a server-client implentation of visualization of filtering algorithm. It has two parts that communicates through
HTTP protocol:

1. server/backend does the actual computation. It collects data from sensor, applies filtering in real time and sends data to the
2. client/frontend does the rendering in browser. The tech of choice is React Function Component + TypeScript + Victory Chart.

## Overview

The backend has a few tasks. It needs to:

- translate all API calls into HTTPS request in `./api/routers/` or `/api/endpoints/`,
- validate HTTP query parameter type and range in `./schemas/`,
- (optional) translate python object into database row in `./models` if postgres/sqlite is used,
- implement custom starup/shutdown and all other stuff related to `FastAPI` in `./core`, and finally
- start the whole backend in `../main.py`.

The overall file structure of backend thus reflects this. For this project, a reduced skeleton of [fastAPI fullstack postgres](https://github.com/eightBEC/fastapi-ml-skeleton) is used:

```bash
.
├── ./app
│   ├── ./app/api               # module for all path operation
│   ├── ./app/core              # common module shared by all other
│   ├── ./app/imufilter         # module for filtering imu signals
│   ├── ./app/__init__.py       # make `app` an python module, thus accessible by `main.py`
│   ├── ./app/random            # skeleton app, obsolete. Keep for old time sake.
│   └── ./app/schemas           # python <-> JSON object validation, using pydantic
├── ./main.py                   # main driver script, the entry point
└── ./README.md                 # this document
```

For starter, look into `./main.py` and start exploring from there.

See [frontend documentation](../frontend/README.md) for how frontend is done.

### Adding New Backend App

To add a new microservice `johnwick` under `./app`, do:

1. create a python module under `./johnwick` (remember `__init__.py`),
2. add path operation under `./api/routers/johnwick.py` and update `./api/routers/router.py`,
3. add JSON schema under `./schemas/johnwick.py` (optionally update `__init__.py`),
4. modify `./core/eventhandler.py` for startup/shutdown.

That is it!

I follow the following rules: submodules under `app.api.routers` and `app.schemas` should mirror the submodules under `app`. The exceptions are `core.config` and `core.event_handler`.

## Design of `imufilter` module

`imufilter` module does not follow the pattern outlined above. It implements three closely related api endpoints, all under one module.

`imufilter` has four threads, which are:

1. sensor, `s`, a custom thread object,
2. controller, `c`, a `pykka.ThreadingActor` object with `put()` and `latest()` methods,
3. viewer, `v`, a `pykka.ThreadingActor` object with `render()` method,
4. frontend, `f`, a `FastAPI.App` object with `GET` endpoints supported.

Sensor will periodically call `c.put(data)` where `c` is a `pykka.ActorProxy` (Pykka uses proxy so that all method calls and attribute access on Actor are transformed into message passing across threads instead).

Controller should implement `put(data)` and `latest(number = 10)` to support adding and reading from the queue.

Viewer will call `result = c.latest().get(); return result` whenever frontend gets a `GET` requests.

The challenge here is that request/reply synchronous model does not really work with asynchronous actor model. For this reason we use `pykka`.

## Using `imufilter` module

the only public facing submodule is `interface.py`.
