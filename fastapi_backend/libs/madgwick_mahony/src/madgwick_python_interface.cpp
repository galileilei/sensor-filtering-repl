#include <pybind11/pybind11.h>
#include "MadgwickAHRS.h"

namespace py = pybind11;

// not sure if a class is needed...
struct Madgwick {
    Madgwick() {};
    void set(float q0_, float q1_, float q2_, float q3_) {
        q0 = q0_;
        q1 = q1_;
        q2 = q2_;
        q3 = q3_;
    }

    float get(int i) {
        switch(i) {
            case 0:
                return q0;
            case 1:
                return q1;
            case 2:
                return q2;
            case 3:
                return q3;
        }
    }

    void updateBeta(float beta_) {
        beta = beta_;
    }

    void updateFreq(float freq_) {
        sampleFreq = freq_;
    }
};

PYBIND11_MODULE(madgwick, m) {
    m.doc() = "Madgwick global variable wrapper";
    py::class_<Madgwick>(m, "Madgwick")
        .def(py::init<>())
        .def("set", &Madgwick::set)
        .def("get", &Madgwick::get)
        .def("updateBeta", &Madgwick::updateBeta)
        .def("updateFreq", &Madgwick::updateFreq);
    m.def("update", &MadgwickAHRSupdate, "Madgwick update with magnetometer");
    m.def("updateIMU", &MadgwickAHRSupdateIMU, "Madgwick update without magnetometer");
}


