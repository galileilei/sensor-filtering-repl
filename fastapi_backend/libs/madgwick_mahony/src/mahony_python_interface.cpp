#include <pybind11/pybind11.h>
#include "MahonyAHRS.h"

namespace py = pybind11;

// not sure if a class is needed...
struct Mahony {
    Mahony() {};
    void set(float q0_, float q1_, float q2_, float q3_) {
        q0 = q0_;
        q1 = q1_;
        q2 = q2_;
        q3 = q3_;
    }

    float get(int i) {
        switch(i) {
            case 0:
                return q0;
            case 1:
                return q1;
            case 2:
                return q2;
            case 3:
                return q3;
        }
    }

    void updateKp(float kp_) {
        twoKp = 2. * kp_;
    }

    void updateKi(float ki_) {
        twoKi = 2. * ki_;
    }

    void updateFreq(float freq_) {
        sampleFreq = freq_;
    }
};

PYBIND11_MODULE(mahony, m) {
    m.doc() = "Mahony global variable wrapper";
    py::class_<Mahony>(m, "Mahony")
        .def(py::init<>())
        .def("set", &Mahony::set)
        .def("get", &Mahony::get)
        .def("updateKp", &Mahony::updateKp)
        .def("updateKi", &Mahony::updateKi)
        .def("updateFreq", &Mahony::updateFreq);
    m.def("update", &MahonyAHRSupdate, "Mahony update with magnetometer");
    m.def("updateIMU", &MahonyAHRSupdateIMU, "Mahony update without magnetometer");
}


