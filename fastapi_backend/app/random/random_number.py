from fastapi import FastAPI
from pydantic import BaseModel

randomNumber = FastAPI()

@randomNumber.get("/api/random")
def read_root():
    return {"Hello": "World"}

@randomNumber.get("/api/random/now")
def generate_random():
    return {"key": "value"}