import uvicorn
from .random_number import randomNumber

if __name__ == "__main__":
    uvicorn.run(randomNumber, host="127.0.0.1", port=8000)