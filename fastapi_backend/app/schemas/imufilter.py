from pydantic import BaseModel
from enum import Enum
from typing import Dict, Tuple, Union

# SENSOR from the phone sensorstream android app
SENSOR_MAP: Dict[int, str] = {
    1: "gps_position",
    3: "accelerometer",
    4: "gyroscope",
    5: "magnetic_field",
    81: "orientation",
    82: "linear_acceleration",
    83: "gravity",
    84: "rotation_vector",
    85: "pressure",
    86: "battery_temperature",
}


class SensorDataType(str, Enum):
    # sensor input
    gps_position = "gps_position"
    accelerometer = "accelerometer"
    gyroscope = "gyroscope"
    magnetic_field = "magnetic_field"
    orientation = "orientation"
    linear_acceleration = "linear_acceleration"
    gravity = "gravity"
    rotation_vector = "rotation_vector"
    pressure = "pressure"
    battery_temperature = "battery_temperature"
    sender = "sender"
    event_timestamp = "event_timestamp"
    # filter output with and without magnetometer, in quaternion
    quat1 = "quat1"
    quat2 = "quat2"
    # filter output with and without magnetometer, in Euler angles
    euler1 = "euler1"
    euler2 = "euler2"
    # gravity cross magnetometer
    gravity_direction = "gravity_direction"
    measured_heading = "measured_heading"
    north_pole = "north_pole"
    # for dummy input, backwards compactibility
    placeholder = "placeholder"


Pair = Tuple[float, float]
_zero_pair: Tuple = (0.0, 0.0)

Vector = Tuple[float, float, float]
# follow three.js convention, makes a ditinction between
# rotation represented as euler angles
# normal vector
_zero_vector: Vector = (0.0, 0.0, 0.0)
_zero_euler: Vector = (0.0, 0.0, 0.0)
_default_gravity: Vector = (0.0, 0.0, -9.80)

Quaternion = Tuple[float, float, float, float]
_default_quat: Quaternion = (0.0, 0.0, 0.0, 1.0)

Measurement = Tuple[float, float, float, float, float, float, float, float, float]


class BackendPayload(BaseModel):
    timestamp: float
    payload: Union[str, float, Pair, Vector, Quaternion]


class SensorPayload(BaseModel):
    gps_location: Pair = _zero_pair
    accelerometer: Vector = _default_gravity
    gyroscope: Vector = _zero_vector
    magnetic_field: Vector = _zero_vector
    orientation: Vector = _zero_vector
    linear_acceleration: Vector = _zero_vector
    gravity: Vector = _default_gravity
    rotation_vector: Vector = _zero_vector
    pressure: float = 0.0
    battery_temperature: float = 30.0
    sender: str = "localhost"
    event_timestamp: float = 0.0
    # filter output with and without magnetometer, in quaternion
    quat1: Quaternion = _default_quat
    quat2: Quaternion = _default_quat
    # filter output with and without magnetometer, in Euler angles
    euler1: Vector = _zero_vector
    euler2: Vector = _zero_vector
    # gravity cross magnetometer
    accelerometer_direction: Vector = _zero_euler
    measured_heading: Vector = _zero_euler
    north_pole: Vector = _zero_euler
    # for dummy input, backwards compactibility
    placeholder: float = 0.0


def idx_to_sensor_type(idx: int) -> SensorDataType:
    global SENSOR_MAP
    return SensorDataType(SENSOR_MAP[idx])
