from typing import Callable

from fastapi import FastAPI
from loguru import logger

from ..imufilter import IMUFilter
from .config import ACTIVE_SENSORS, ACTIVE_FILTERS


def start_app_handler(app: FastAPI) -> Callable:
    def startup() -> None:
        logger.info("Running app start handler\n")
        logger.add("./log.txt")
        app.state.models = dict()
        global ACTIVE_SENSORS
        global ACTIVE_FILTERS
        for s in ACTIVE_SENSORS:
            for f in ACTIVE_FILTERS:
                app.state.models[(s, f)] = IMUFilter(s, f)

    return startup


def stop_app_handler(app: FastAPI) -> Callable:
    def stop() -> None:
        logger.info("Running app shutdown handler\n")
        global ACTIVE_SENSORS
        global ACTIVE_FILTERS
        for s in ACTIVE_SENSORS:
            for f in ACTIVE_FILTERS:
                app.state.models[(s, f)].stop()

    return stop
