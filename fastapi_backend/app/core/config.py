# TODO: encapsulate them in classes to avoid
# potential name collision down the road
ALLOW_ORIGIN = ["http://localhost:3000"]

# for phone
PHONE_IP = ""  # Must be empty string! "localhost" does not work.
PHONE_PORT = 5555
_LOCAL_IP = "10.1.194.76", "192.168.168.4"  # first for CT_DEV, second for howen

# for startup options
"""
MOCK_APP_ALLOWED = False
PHONE_MADGWICK_ALLOWED = True
DEVICE_MADGWICK_ALLOWED = False
PHONE_MAHONY_ALLOWED = True
DEVICE_MAHONY_ALLOWED = True
"""
ACTIVE_SENSORS = ["phone"]
ACTIVE_FILTERS = ["madgwick", "mahony"]

# for device
SERIAL_PORT = "/dev/ttyUSB0"
_DENOM = 1 << 15
# see ..imufilter.sensor:CartrackSensor._get_payload()
ACCEL_FACTOR = 16.0 * 9.80 / _DENOM  # unit is m s^{-2}
GYRO_FACTOR = 250.0 / _DENOM  # unit is deg s^{-1}
MAG_FACTOR = 0.3e-2  # unit is gauss instead
# MAG_FACTOR = 0.3e-6  # unit is tesla
