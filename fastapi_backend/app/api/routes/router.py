from fastapi import APIRouter, Request
from ...schemas import SensorDataType, SensorPayload
from ...imufilter import MySensorType, MyFilterType
from loguru import logger
from typing import List

router = APIRouter()


@router.get(
    "/latest/{sensor}/{filters}",
    response_model=List[SensorPayload],
    name="queryLatest",
)
def queryLatest(request: Request, sensor: MySensorType, filters: MyFilterType):
    model = app.state.models[(sensor, filters)]
    result = model.queryLatest()
    logger.debug(f"result is {result} in queryLatest()")
    return result


@router.get(
    "/since/{sensor}/{filters}/{since}",
    response_model=List[SensorPayload],
    name="queryLatest",
)
def querySince(
    request: Request, sensor: MySensorType, filters: MyFilterType, since: float
):
    model = app.state.models[(sensor, filters)]
    result = model.querySince(since)
    logger.debug(f"result is {result} in querySince()")
    return result
