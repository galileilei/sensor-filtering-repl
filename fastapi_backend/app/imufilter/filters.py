from pykka import ThreadingActor
from collections import deque
from loguru import logger
from typing import List, Tuple, Deque, Literal
from ..schemas import SensorPayload, Quaternion, Measurement
from . import mahony, madgwick
from math import pi as PI
from .quaternion import Q2R, P2R, R2Q, cross_product, to3js


class GenericFilter(ThreadingActor):
    """
    Interface to Filter class, it support two public API:
    * def put(self, payload) for sensor to put data in
    * def latest(self, dtype, number) for view to poll data
    To use the interface, you MUST IMPLEMENT THIS:
    * def _filter(self, payload)
    """

    _default_value = [0.0, 0.0, 0.0]
    _deque_maxlen = 200

    def __init__(self):
        super().__init__()
        self.queue: Deque[SensorPayload] = deque(maxlen=self._deque_maxlen)
        logger.debug("Generic filter started")

    def put(self, payload: SensorPayload) -> None:
        res = self._filter(payload)
        self.queue.append(res)
        logger.debug("append a new payload")

    def queryLatest(self, number: int = 5) -> List[SensorPayload]:
        length = len(self.queue)
        size = length if length < number else number
        logger.debug(f"size = {size} in latest()")
        return self.queue[-size:]

    def querySince(self, timestamp: float = -1.0) -> List[SensorPayload]:
        if timestamp < 0:
            return self.latest(number=5)
        # a hack here, don't grab more than 20?
        length = len(self.queue)
        size = length if length < 20 else 20
        temp = self.queue[-size:]
        return [x for x in temp if x.event_timestamp > timestamp]

    def _filter(self, payload: SensorPayload) -> SensorPayload:
        # TODO: add default behaviour here ?
        raise NotImplementedError


class NoFilter(GenericFilter):
    """
    No filter simply passes data through
    """

    def _filter(self, payload: SensorPayload) -> SensorPayload:
        return payload


_unit_quaternion = (1, 0, 0, 0)


class QuaternionFilter(GenericFilter):
    """
    Common part of the two Quaternion-based filters,
    Madgwick and Mahony. Notably the _filter() method.
    To use this interface, YOU MUST:
    1. give a model that support .get() and .set() to access the quaternion.
    2. implement _update(meausrement) to update the quaternion.
    """

    _life_line: float = 10.0

    def __init__(self, model):
        super().__init__()
        self.last_time = None
        self.model = model
        self.quat1: Quaternion = _unit_quaternion
        self.quat2: Quaternion = _unit_quaternion

    def _filter(self, payload: SensorPayload) -> SensorPayload:
        """
        common filter logic only for madgwick/mahony filters
        """
        if self._skip_filter(payload):
            return self._init_filter(payload)
        measurement1, measurement2 = self._unpack_payload(payload)
        payload = self._pre_filter(payload)
        logger.debug(f"before update, quat1 is {self.quat1}")
        self._set_state(self.quat1)
        self.quat1 = self._update(measurement1)
        logger.debug(f"after update, quat1 is {self.quat1}")
        self._set_state(self.quat2)
        self.quat2 = self._update(measurement2)
        payload = self._post_filter(payload)
        return payload

    def _get_state(self) -> Quaternion:
        return tuple(self.model.get(i) for i in range(4))

    def _set_state(self, q: Quaternion) -> None:
        self.model.set(*q)

    def _update(self, m: Measurement) -> Quaternion:
        raise NotImplementedError

    def _pre_filter(self, payload: SensorPayload) -> SensorPayload:
        dt = payload.event_timestamp - self.last_time
        self.model.updateFreq(1.0 / dt)
        return payload

    ## TODO: measured_heading has /0 issue when staerting up
    def _post_filter(self, payload: SensorPayload) -> SensorPayload:
        self.last_time = payload.event_timestamp
        payload.quat1 = to3js(self.quat1)
        payload.quat2 = to3js(self.quat2)
        payload.euler1 = Q2R(self.quat1)
        payload.euler2 = Q2R(self.quat2)
        direction = P2R(payload.accelerometer)
        payload.measured_heading = P2R(
            cross_product(payload.magnetic_field, payload.accelerometer)
        )
        payload.accelerometer_direction = direction
        payload.orientation = tuple(x / 180.0 * PI for x in payload.orientation)
        payload.north_pole = P2R(payload.magnetic_field)
        return payload

    def _init_filter(self, payload: SensorPayload) -> SensorPayload:
        """
        running the filter for the first time
        TODO: checkout comma.ai rednose filter initialization
        TODO: initialize filter based on measured heading?
        """
        # self.quat1 = R2Q(payload.rotation_vector)
        # self.quat2 = R2Q(payload.rotation_vector)

        # calculate measured heading
        mag, acc = payload.magnetic_field, payload.accelerometer
        direction = P2R(acc)
        payload.measured_heading = P2R(cross_product(mag, acc))
        payload.accelerometer_direction = direction
        payload.orientation = tuple(x / 180.0 * PI for x in payload.orientation)
        payload.north_pole = P2R(mag)

        # use measured heading to initialize
        self.quat1 = R2Q(payload.measured_heading)
        self.quat2 = R2Q(payload.measured_heading)
        payload = self._post_filter(payload)
        return payload

    def _skip_filter(self, payload: SensorPayload) -> bool:
        """
        skip filtering if just starting, or last data point
        has expired.
        """
        if self.last_time is None:
            return True
        ts = payload.event_timestamp
        dt = ts - self.last_time
        if dt > self._life_line:
            return True
        return False

    def _unpack_payload(
        self, payload: SensorPayload
    ) -> Tuple[Measurement, Measurement]:
        """
        utility function only for madgwick/mahony filter
        """
        acc = payload.accelerometer
        gyro = payload.gyroscope
        mag = payload.magnetic_field
        logger.debug(f"payload unpacked as {gyro}, {acc}, {mag}")
        result = (*gyro, *acc, *mag), (*gyro, *acc, 0.0, 0.0, 0.0)
        return result


class MahonyFilter(QuaternionFilter):
    """
    Mahony Filter
    """

    def __init__(self, model=mahony.Mahony()):
        super().__init__(model)
        logger.debug("Mahony filter started")

    def _update(self, m: Measurement) -> Quaternion:
        mahony.update(*m)
        return self._get_state()


class MadgwickFilter(QuaternionFilter):
    """
    Madgwick Filter
    """

    def __init__(self, model=madgwick.Madgwick()):
        super().__init__(model)
        logger.debug("Madgwick filter started")

    def _update(self, m: Measurement) -> Quaternion:
        madgwick.update(*m)
        return self._get_state()


MyFilterType = Literal["mock", "madgwick", "mahony"]


def get_filter(key: MyFilterType) -> GenericFilter:
    if key == "mock":
        return NoFilter()
    if key == "madgwick":
        return MadgwickFilter()
    if key == "mahony":
        return MahonyFilter()
    logger.error(f"this key, {key}, is not supported")
    raise NotImplementedError
