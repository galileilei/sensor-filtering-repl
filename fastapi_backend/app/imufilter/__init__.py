from . import sensor, filters
from .sensor import MySensorType
from .filters import MyFilterType


class IMUFilter:
    def __init__(self, sensor_type: MySensorType, filter_type: MyFilterType):
        self._filter = filters.get_filter(filter_type).start()
        self.proxy = self._filter.proxy()
        self.sensor = sensor.connect_sensor(sensor_type, self.proxy)

    def stop(self):
        self._filter.stop()
        self.sensor.stop(self.proxy)

    def queryLatest(self, number: int):
        payload = self.filter.queryLatest(number).get()
        return payload

    def querySince(self, timestamp: float):
        payload = self.filter.querySince(timestamp).get()
        return payload


__all__ = ["MySensorType", "MyFilterType", "GenericInterface", "IMUFilter"]