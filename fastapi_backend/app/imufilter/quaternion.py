"""
Conversion between different representations of rotation:
1. as a tuple of Euler angles, R,
2. as a quaternion, Q, and
3. as a unit vector, P.
But mostly quaternions.
"""

import math
from ..schemas import Quaternion, Vector


def _norm(r: Vector) -> float:
    x, y, z = r
    return math.sqrt(x * x + y * y + z * z)


def R2Q(r: Vector) -> Quaternion:
    """
    given an rotation expressed as euler angle in radian, (yaw, pitch, roll),
    return a quaternion (w, x, y, z)
    """
    norm = _norm(r)
    if norm < 1e-8:
        return (1.0, 0.0, 0.0, 0.0)
    else:
        v = math.cos(norm / 2.0)
        x, y, z = tuple(x / norm * math.sin(norm / 2.0) for x in r)
        return (v, x, y, z)


def Q2R(q: Quaternion) -> Vector:
    """
    Given a quaternion q = (w, x, y, z),
    return a rotation as Euler angles in radian, (yaw, pitch, roll)
    """
    v, x, y, z = q
    norm = _norm((x, y, z))
    if norm < 1e-8:
        return (0.0, 0.0, 0.0)
    elif v >= 0.0:
        return tuple(2.0 * math.acos(v) * x / norm for x in (x, y, z))
    else:
        return tuple(-2.0 * math.acos(-v) * x / norm for x in (x, y, z))


def cross_product(x: Vector, y: Vector) -> Vector:
    """
    Given two non-zero vectors, returns a direction as P (not R or Q)!
    see the following link:
    https://en.wikipedia.org/wiki/Cross_product#Coordinate_notation
    """
    a1, a2, a3 = x
    b1, b2, b3 = y
    s1 = a2 * b3 - a3 * b2
    s2 = a3 * b1 - a1 * b3
    s3 = a1 * b2 - a2 * b1
    return (s1, s2, s3)


def P2R(p: Vector) -> Vector:
    """
    Given a position vector p = (x, y, z) with r = norm(p),
    find a rotation as euler angles in radian, (yaw, pitch, roll),
    such that it rotates (r, 0, 0) to (x, y, z).
    roll == 0.0 always.
    see the following link:
    https://en.wikipedia.org/wiki/Rotation_matrix#General_rotations
    """
    x, y, z = p
    r = _norm(p)
    if r < 1e-8:
        return (0.0, 0.0, 0.0)
    pitch = math.asin(-z / r)
    yaw = math.atan2(y, x)
    return (yaw, pitch, 0.0)


def to3js(q: Quaternion) -> Quaternion:
    """
    Threejs uses (x, y, z, w). Filter uses (w, x, y, z).
    """
    w, x, y, z = q
    return x, y, z, w
