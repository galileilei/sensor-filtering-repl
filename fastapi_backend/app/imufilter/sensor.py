"""
Collect sensor data

To ensure only one instance of a sensor is running at anytime, 
ONLY USE connect_sensor("mock", filter).
"""
from .filters import GenericFilter
from time import sleep
from threading import Thread
import socket
import serial
import numpy as np

from ..core.config import (
    PHONE_PORT,
    PHONE_IP,
    SERIAL_PORT,
    ACCEL_FACTOR,
    MAG_FACTOR,
    GYRO_FACTOR,
)
from ..schemas import idx_to_sensor_type, SensorPayload, SENSOR_MAP
from loguru import logger
from typing import List, Literal


class MockSensor(Thread):
    """
    a mock sensor, mimicking a real sensor by
    sending a payload with only one field, "placeholder"
    """

    def __init__(self, receivers: List[GenericFilter] = [], refresh_rate: float = 0.4):
        Thread.__init__(self)
        self._died = False
        self._refresh_rate = refresh_rate
        self._receivers = receivers
        self._counter = 0
        self.start()
        logger.debug("mock sensor started")

    def run(self):
        while not self._died:
            sleep(self._refresh_rate)
            payload = self._get_payload()
            for r in self._receivers:
                r.put(payload).get()
            logger.debug(f"put payload({payload}) in mock controller")

    def stop(self, receiver: GenericFilter):
        self._receivers.remove(receiver)
        if not self._receivers:
            self._died = True

    def _get_payload(self) -> SensorPayload:
        payload = SensorPayload(
            event_timestamp=self._counter, placeholder=np.random.randn(1)
        )
        self._counter = self._counter + 1
        return payload

    def add_receiver(self, receiver: GenericFilter):
        self._receivers.append(receiver)


class PhoneSensor(Thread):
    """
    reading data from a phone IMU+GPS unit.
    DO NOT CALL this sensor constructor directly! use connect_sensor() instead.
    """

    def __init__(
        self,
        receivers: List[GenericFilter] = [],
        debug=False,
        ip=PHONE_IP,
        port=PHONE_PORT,
    ):
        Thread.__init__(self, daemon=True)
        self._died = False
        self._debug = debug
        self._receivers = receivers
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.bind((ip, port))
        self.start()
        logger.debug(f"phone sensor started, {ip}, {port}")

    def run(self):
        while not self._died:
            logger.debug("waiting for raw data")
            data, addr = self._sock.recvfrom(4048)
            payload = self._get_payload(data)
            for r in self._receivers:
                r.put(payload).get()
            logger.debug(
                f"phone sensor put value in phone controller, value is {payload}"
            )

    @staticmethod
    def _get_payload(data: bytes, idx=SENSOR_MAP) -> SensorPayload:
        """
        convert a bytestring b'' into SensorPayload.
        """
        logger.debug(f"raw msg is {data}")
        row = data.decode("utf-8").split(",")
        tokens: List[float] = [float(x) for x in row]
        payload = {}
        # this is really bad, non-pythonic code
        for key in idx:
            if key in tokens:
                _index: int = tokens.index(key)
                if key not in (85, 86):
                    payload[idx_to_sensor_type(key)] = tuple(
                        tokens[_index + 1 : _index + 4]
                    )
                else:
                    payload[idx_to_sensor_type(key)] = tuple(
                        tokens[_index + 1 : _index + 2]
                    )
        payload["event_timestamp"] = tokens[0]
        return SensorPayload.parse_obj(payload)

    def stop(self, receiver: GenericFilter):
        self._receivers.remove(receiver)
        if not self._receivers:
            self._died = True

    def add_receiver(self, receiver: GenericFilter):
        self._receivers.append(receiver)


class CartrackTimestamp:
    """
    Estimate a timestamp from cartrack device timestamp.

    Reading from cratrack device is as followed:
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888327.0
    318888389.0

    The observed timestamp would normally repeat 9 times before incrementing by
    60 /pm 1 ms.
    """

    _dt_in_ms = 7

    def __init__(self):
        self.observable = None
        self.repeat = None
        self.hidden = None

    def update(self, val):
        # TODO: refactor this monster
        if self.hidden is None:
            self.observable = val
            self.hidden = val
            self.repeat = 0
        elif self.observable == val:
            self.hidden += self._dt_in_ms
            self.repeat += 1
        else:
            self.hidden = val
            self.repeat = 0
            self.observable = val

    def get(self):
        return self.hidden


class CartrackSensor(Thread):
    """
    reading data from cartrack device
    DO NOT CALL this sensor constructor directly! Use connect_sensor() instead.
    """

    def __init__(self, receivers: List[GenericFilter] = [], _port=SERIAL_PORT):
        Thread.__init__(self, daemon=True)
        self._died = False
        self._receivers = receivers
        self._port = serial.Serial(port=_port, baudrate=115200)
        self._timestamp = CartrackTimestamp()
        self.start()
        logger.debug(f"device sensor started, port is {SERIAL_PORT}")

    def run(self):
        while not self._died:
            ##logger.debug("in device sensor loop")
            if self._port.in_waiting > 0:
                serialString = self._port.readline()
                # TODO: process the data into json object
                payload = self._get_payload(serialString)
                for r in self._receivers:
                    r.put(payload).get()
                logger.debug(f"put {payload} into filter")
                # send(self._receiver, ["data", serialString])

    def stop(self, receiver: GenericFilter):
        self._receivers.remove(receiver)
        if not self._receivers:
            self._died = True

    def _get_payload(self, data: bytes) -> SensorPayload:
        """
        convert a byte string b'' into SensorPayload.
        this is duplicate code from
        http://bitbucket.com/cartrack/accidents.git:src/imu.py:

        Example:
        > data = b'\r18,3,1c,52,a,f82e,fce9,fc21,8d,65bd,88180\n'
        after cleaning up, we have
        > tokens = [24, 3, 28, 82, 10, 63534, 64745, 64545, 141, 26045, 88180]
        whhere each slot is 16-bit word defined as:
        (Update 28 Jan 2020: the timestamp field IS NOT 16-BIT)
        [Gx, Gy, Gz, Ax, Ay, Az, Mx, My, Mz, Hall, Timestamp]
        where
        G_ is between [-250 deg/seconds, 250 deg/seconds],
        A_ is between [-16G, 16G],
        M_ is 3 x 10 ^{-6} tesla per bit, or 3 * 10^{-2} gauss per bit,
        Hall effect is not used,
        Timestamp is not used.
        """
        row: List[str] = data.decode("ascii").split(",")
        logger.debug(f"raw input is {row}")
        # ugly hacks to remote leading /r and ending /n
        row[0] = row[0][1:]
        row[-1] = row[-1][:-1]
        tokens: List[int] = [hex2int(x) for x in row[:-2]]
        payload = dict()
        payload["gyroscope"] = [x * GYRO_FACTOR for x in tokens[:3]]
        payload["accelerometer"] = [x * ACCEL_FACTOR for x in tokens[3:6]]
        payload["magnetic_field"] = [x * MAG_FACTOR for x in tokens[6:9]]
        observed_ts = int(row[-1], base=10)
        self._timestamp.update(observed_ts)
        payload["event_timestamp"] = self._timestamp.get()
        return SensorPayload.parse_obj(payload)

    def add_receiver(self, receiver: GenericFilter):
        self._receivers.append(receiver)


def hex2int(hexstr: str, bits: int = 16) -> int:
    """
    convert a hex string to signed int, default base = 16.
    Assumes that the highest bit is used for sign.
    https://stackoverflow.com/questions/6727875/hex-string-to-signed-int-in-python-3-2
    """
    value = int(hexstr, 16)
    if value & (1 << (bits - 1)):
        value -= 1 << bits
    return value


# Global variable: only allow one sensor at a time
MySensorType = Literal["mock", "phone", "cartrack"]


GLOBAL_SENSORS = dict()


def start_sensor(key: MySensorType):
    if key == "mock":
        GLOBAL_SENSORS["mock"] = MockSensor()
    elif key == "phone":
        GLOBAL_SENSORS["phone"] = PhoneSensor()
    elif key == "cartrack":
        GLOBAL_SENSORS["cartrack"] = CartrackSensor()
    else:
        logger.error("only mock, phone and cartrack sensors are supported")
        raise NotImplementedError


def connect_sensor(key: MySensorType, receiver: GenericFilter):
    """
    To ensure only one instance of a sensor is running at anytime, ONLY USE connect_sensor("mock", filter).
    """
    if GLOBAL_SENSORS.get(key) is None:
        start_sensor(key)
    GLOBAL_SENSORS[key].add_receiver(receiver)
    return GLOBAL_SENSORS[key]
